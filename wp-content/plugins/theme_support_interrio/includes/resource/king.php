<?php
/**
 * Kingcomposer array
 *
 * @package Student WP
 * @author Shahbaz Ahmed <shahbazahmed9@hotmail.com>
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( 'Restricted' );
}

$orderby = array(
				"type"			=>	"select",
				"label"			=>	esc_html__("Order By", BUNCH_NAME),
				"name"			=>	"sort",
				'options'		=>	array('date'=>esc_html__('Date', BUNCH_NAME),'title'=>esc_html__('Title', BUNCH_NAME) ,'name'=>esc_html__('Name', BUNCH_NAME) ,'author'=>esc_html__('Author', BUNCH_NAME),'comment_count' =>esc_html__('Comment Count', BUNCH_NAME),'random' =>esc_html__('Random', BUNCH_NAME) ),
				"description"	=>	esc_html__("Enter the sorting order.", BUNCH_NAME)
			);
$order = array(
				"type"			=>	"select",
				"label"			=>	esc_html__("Order", BUNCH_NAME),
				"name"			=>	"order",
				'options'		=>	(array('ASC'=>esc_html__('Ascending', BUNCH_NAME),'DESC'=>esc_html__('Descending', BUNCH_NAME) ) ),			
				"description"	=>	esc_html__("Enter the sorting order.", BUNCH_NAME)
			);
$number = array(
				"type"			=>	"text",
				"label"			=>	esc_html__('Number', BUNCH_NAME ),
				"name"			=>	"num",
				"description"	=>	esc_html__('Enter Number of posts to Show.', BUNCH_NAME )
			);
$text_limit = array(
				"type"			=>	"text",
				"label"			=>	esc_html__('Text Limit', BUNCH_NAME ),
				"name"			=>	"text_limit",
				"description"	=>	esc_html__('Enter text limit of posts to Show.', BUNCH_NAME )
			);
$title = array(
				"type"			=>	"text",
				"label"			=>	esc_html__('Title', BUNCH_NAME ),
				"name"			=>	"title",
				"description"	=>	esc_html__('Enter section title.', BUNCH_NAME )
			);
$subtitle = array(
				"type"			=>	"text",
				"label"			=>	esc_html__('Sub-Title', BUNCH_NAME ),
				"name"			=>	"subtitle",
				"description"	=>	esc_html__('Enter section subtitle.', BUNCH_NAME )
			);
$text = array(
				"type"			=>	"textarea",
				"label"			=>	esc_html__('Text', BUNCH_NAME ),
				"name"			=>	"text",
				"description"	=>	esc_html__('Enter text to show.', BUNCH_NAME )
			);
$editor  = array(
				"type"			=>	"editor",
				"label"			=>	esc_html__('Title', BUNCH_NAME ),
				"name"			=>	"editor",
				"description"	=>	esc_html__('Enter title to show.', BUNCH_NAME )
			);
$email = array(
				"type"			=>	"text",
				"label"			=>	esc_html__('Email', BUNCH_NAME ),
				"name"			=>	"email",
				"description"	=>	esc_html__('Enter email.', BUNCH_NAME )
			);
$phone = array(
				"type"			=>	"text",
				"label"			=>	esc_html__('Phone', BUNCH_NAME ),
				"name"			=>	"phone",
				"description"	=>	esc_html__('Enter phone.', BUNCH_NAME )
			);
$address = array(
				"type"			=>	"text",
				"label"			=>	esc_html__('Address', BUNCH_NAME ),
				"name"			=>	"address",
				"description"	=>	esc_html__('Enter address.', BUNCH_NAME )
			);
$working_hours = array(
				"type"			=>	"text",
				"label"			=>	esc_html__('Working Hours', BUNCH_NAME ),
				"name"			=>	"working_hours",
				"description"	=>	esc_html__('Enter Working Hours.', BUNCH_NAME )
			);
$contact_title = array(
				"type"			=>	"text",
				"label"			=>	esc_html__('Contact Us Title', BUNCH_NAME ),
				"name"			=>	"contact_title",
				"description"	=>	esc_html__('Enter contact us title.', BUNCH_NAME )
			);
$contact_text = array(
				"type"			=>	"textarea",
				"label"			=>	esc_html__('Contact Us Shortcode', BUNCH_NAME ),
				"name"			=>	"contact_text",
				"description"	=>	esc_html__('Enter contact us shortcode to show.', BUNCH_NAME )
			);
$latitude = array(
				"type"			=>	"text",
				"label"			=>	esc_html__('Latitude', BUNCH_NAME ),
				"name"			=>	"latitude",
				"description"	=>	esc_html__('Enter latitude.', BUNCH_NAME )
			);
$longitude = array(
				"type"			=>	"text",
				"label"			=>	esc_html__('Longitude', BUNCH_NAME ),
				"name"			=>	"longitude",
				"description"	=>	esc_html__('Enter longitude.', BUNCH_NAME )
			);
$zoom = array(
				"type"			=>	"text",
				"label"			=>	esc_html__('Map Zoom', BUNCH_NAME ),
				"name"			=>	"zoom",
				"description"	=>	esc_html__('Enter map zoom.', BUNCH_NAME )
			);
$btn_title = array(
				"type"			=>	"text",
				"label"			=>	esc_html__('Button Title', BUNCH_NAME ),
				"name"			=>	"btn_title",
				"description"	=>	esc_html__('Enter section Button title.', BUNCH_NAME )
			);
$btn_link = array(
				"type"			=>	"text",
				"label"			=>	esc_html__('Button Link', BUNCH_NAME ),
				"name"			=>	"btn_link",
				"description"	=>	esc_html__('Enter section Button Link.', BUNCH_NAME )
			);
$img = array(
				"type"			=>	"attach_image_url",
				"label"			=>	esc_html__('Image', BUNCH_NAME ),
				"name"			=>	"img",
				'admin_label' 	=> 	false,
				"description"	=>	esc_html__('Choose Image.', BUNCH_NAME )
			);
$bg_img = array(
				"type"			=>	"attach_image_url",
				"label"			=>	esc_html__('Background Image', BUNCH_NAME ),
				"name"			=>	"bg_img",
				'admin_label' 	=> 	false,
				"description"	=>	esc_html__('Choose Background image.', BUNCH_NAME )
			);
$multi_img = array(
				"type"			=>	"attach_images",
				"label"			=>	esc_html__('Multi Images', BUNCH_NAME ),
				"name"			=>	"multi_img",
				'admin_label' 	=> 	false,
				"description"	=>	esc_html__('Uplod multi images.', BUNCH_NAME )
			);
$signature = array(
				"type"			=>	"attach_image_url",
				"label"			=>	esc_html__('Signature', BUNCH_NAME ),
				"name"			=>	"signature",
				"description"	=>	esc_html__('Choose Signature.', BUNCH_NAME )
			);
$video = array(
				"type"			=>	"text",
				"label"			=>	esc_html__('Video URL', BUNCH_NAME ),
				"name"			=>	"video",
				'admin_label' 	=> 	false,
				"description"	=>	esc_html__('Enter video url.', BUNCH_NAME )
			);
$video_img = array(
				"type"			=>	"attach_image_url",
				"label"			=>	esc_html__('Video Image', BUNCH_NAME ),
				"name"			=>	"video_img",
				'admin_label' 	=> 	false,
				"description"	=>	esc_html__('Choose video image.', BUNCH_NAME )
			);
$icon = array(
				'type' => 'icon_picker',
				'label' => esc_html__('Icon', BUNCH_NAME ),
				'name' => 'icon',
				'description' => esc_html__('Enter your icon', BUNCH_NAME )
			);
$url = array(
				'type' => 'text',
				'label' => esc_html__('URL', BUNCH_NAME ),
				'name' => 'url',
				'description' => esc_html__('Enter url', BUNCH_NAME ),
				'value'	=> '#',
			);
$counter_start = array(
				'type' => 'text',
				'label' => esc_html__('Counter Start', BUNCH_NAME ),
				'name' => 'counter_start',
				'description' => esc_html__('Enter Counter Start', BUNCH_NAME )
			);
$counter_end = array(
				'type' => 'text',
				'label' => esc_html__('Counter End', BUNCH_NAME ),
				'name' => 'counter_end',
				'description' => esc_html__('Enter Counter End', BUNCH_NAME )
			);
$counter_sign = array(
				'type' => 'text',
				'label' => esc_html__('Counter Sign', BUNCH_NAME ),
				'name' => 'counter_sign',
				'description' => esc_html__('Enter Counter Sign', BUNCH_NAME )
			);
$date = array(
				'type' => 'text',
				'label' => esc_html__('Date', BUNCH_NAME ),
				'name' => 'date',
				'description' => esc_html__('Enter Date', BUNCH_NAME )
			);

$options = array();


// Revslider Start.
$options['bunch_revslider']	=	array(
					'name' => esc_html__('Revslider', BUNCH_NAME),
					'base' => 'bunch_revslider',
					'class' => '',
					'category' => esc_html__('Arctica', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show  Revolution slider.', BUNCH_NAME),
					'params' => array(
						array(
							'type' => 'dropdown',
							'label' => esc_html__('Choose Slider', BUNCH_NAME ),
							'name' => 'slider_slug',
							'options' => bunch_get_rev_slider( 0 ),
							'description' => esc_html__('Choose Slider', BUNCH_NAME )
						),

					),
			);
//Welcome to Interrio
$options['bunch_welcome_to_interrio']	=	array(
					'name' => esc_html__('Welcome to Interrio', BUNCH_NAME),
					'base' => 'bunch_welcome_to_interrio',
					'class' => '',
					'category' => esc_html__('Interrio', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show Welcome to Interrio on Home.', BUNCH_NAME),
					'params' => array(
						$editor,
						$subtitle,
						$text,
						$video,
						$video_img,
						$img,
						$btn_title,
						$btn_link,
						$email,
					),
			);
//Services We Do
$options['bunch_services_we_do'] = array(
					'name' => esc_html__('Services We Do', BUNCH_NAME),
					'base' => 'bunch_services_we_do',
					'class' => '',
					'category' => esc_html__('Interrio', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show Services on homepage.', BUNCH_NAME),
					'params' => array(
						$bg_img,
						$title,
						$text_limit,
						$number,
						array(
							"type" => "dropdown",
							"label" => __( 'Category', BUNCH_NAME),
							"name" => "cat",
							"options" =>  bunch_get_categories(array( 'taxonomy' => 'services_category'), true),
							"description" => __( 'Choose Category.', BUNCH_NAME)
						),
						$orderby,
						$order,
					),
			);
//Latest Projects
$options['bunch_latest_projects'] = array(
					'name' => esc_html__('Latest Projects', BUNCH_NAME),
					'base' => 'bunch_latest_projects',
					'class' => '',
					'category' => esc_html__('Interrio', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show Latest Projects on homepage.', BUNCH_NAME),
					'params' => array(
						$editor,
						$number,
						array(
						   "type" => "textfield",
						   "label" => __('Excluded Categories ID', BUNCH_NAME ),
						   "name" => "exclude_cats",
						   "description" => __('Enter Excluded Categories ID seperated by commas(13,14).', BUNCH_NAME )
						),
						$orderby,
						$order,
					),
			);
//Call Out Home
$options['bunch_call_out_home']	=	array(
					'name' => esc_html__('Call Out Home', BUNCH_NAME),
					'base' => 'bunch_call_out_home',
					'class' => '',
					'category' => esc_html__('Interrio', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show Call Out Home.', BUNCH_NAME),
					'params' => array(
						$bg_img,
						$text,
					),
			);
//Our Working Process
$options['bunch_our_working_process']	=	array(
					'name' => esc_html__('Our Working Process', BUNCH_NAME),
					'base' => 'bunch_our_working_process',
					'class' => '',
					'category' => esc_html__('Interrio', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show Our Working Process Home.', BUNCH_NAME),
					'params' => array(
						$title,
						$text,
						array(
							'type' => 'group',
							'label' => esc_html__( 'Our Working Process Details', BUNCH_NAME ),
							'name' => 'working_process',
							'description' => esc_html__( 'Enter Our Working Process.', BUNCH_NAME ),
							'params' => array(
								$icon,
								$title,
								$text,
							),
						),
					),
			);
//Customer Feedback
$options['bunch_customer_feedback'] = array(
					'name' => esc_html__('Customer Feedback', BUNCH_NAME),
					'base' => 'bunch_customer_feedback',
					'class' => '',
					'category' => esc_html__('Interrio', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show Customer Feedback Home.', BUNCH_NAME),
					'params' => array(
						$bg_img,
						$title,
						$text_limit,
						$number,
						array(
							"type" => "dropdown",
							"label" => __('Category', BUNCH_NAME),
							"name" => "cat",
							"options" =>  bunch_get_categories(array( 'taxonomy' => 'testimonials_category'), true),
							"description" => __( 'Choose Category.', BUNCH_NAME)
						),
						$orderby,
						$order,
					),
			);
//Latest News
$options['bunch_latest_news']	=	array(
					'name' => esc_html__('Latest News', BUNCH_NAME),
					'base' => 'bunch_latest_news',
					'class' => '',
					'category' => esc_html__('Interrio', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show Latest News.', BUNCH_NAME),
					'params' => array(
						$editor,
						$text_limit,
						$number,
						array(
							"type" => "dropdown",
							"label" => __('Category', BUNCH_NAME),
							"name" => "cat",
							"options" =>  bunch_get_categories(array('taxonomy' => 'category'), true),
							"description" => __('Choose Category.', BUNCH_NAME)
						),
						$orderby,
						$order,
						$btn_title,
						$btn_link,
					),
			);
// Clients Home
$options['bunch_clients']	=	array(
					'name' => esc_html__('Clients', BUNCH_NAME),
					'base' => 'bunch_clients',
					'class' => '',
					'category' => esc_html__('Interrio', BUNCH_NAME),
					'icon' => 'fa-users' ,
					'description' => esc_html__('Show Clients Logo images.', BUNCH_NAME),
					'params' => array(
						array(
							'type' => 'group',
							'label' => esc_html__( 'Clients Logo', BUNCH_NAME ),
							'name' => 'our_clients',
							'description' => esc_html__( 'Enter Clients Logo', BUNCH_NAME ),
							'params' => array(
								$title,
								$img,
								$btn_link,
							),
						),
					),
			);
//Our Services
$options['bunch_our_services'] = array(
					'name' => esc_html__('Our Services', BUNCH_NAME),
					'base' => 'bunch_our_services',
					'class' => '',
					'category' => esc_html__('Interrio', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show our Services.', BUNCH_NAME),
					'params' => array(
						$text_limit,
						$number,
						array(
							"type" => "dropdown",
							"label" => __( 'Category', BUNCH_NAME),
							"name" => "cat",
							"options" =>  bunch_get_categories(array( 'taxonomy' => 'services_category'), true),
							"description" => __( 'Choose Category.', BUNCH_NAME)
						),
						$orderby,
						$order,
					),
			);
//Call to Action
$options['bunch_call_to_action']	=	array(
					'name' => esc_html__('Call to Action', BUNCH_NAME),
					'base' => 'bunch_call_to_action',
					'class' => '',
					'category' => esc_html__('Interrio', BUNCH_NAME),
					'icon' => 'fa-cog' ,
					'description' => esc_html__('Show Call to Action.', BUNCH_NAME),
					'params' => array(
						$bg_img,
						$text,
						$btn_title,
						$btn_link
					),
			);
// Services Single 1.
$options['bunch_services_single_1']	=	array(
					'name' => esc_html__('Services Single 1', BUNCH_NAME),
					'base' => 'bunch_services_single_1',
					'class' => '',
					'category' => esc_html__('Interrio', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show Services Single 1.', BUNCH_NAME),
					'params' => array(
						array(
							'type' => 'group',
							'label' => esc_html__( 'Services Single Details', BUNCH_NAME ),
							'name' => 'services_single_1',
							'description' => esc_html__( 'Enter Services Single detail.', BUNCH_NAME ),
							'params' => array(
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Tab Name', BUNCH_NAME ),
									"name"			=>	"tab_name",
									'admin_label' 	=> 	false,
									"description"	=>	esc_html__('Enter tab name.', BUNCH_NAME )
								),
								$multi_img,
								$title,
								$text,
							),
						),
						
					),
			);
// Services Single 2.
$options['bunch_services_single_2']	=	array(
					'name' => esc_html__('Services Single 2', BUNCH_NAME),
					'base' => 'bunch_services_single_2',
					'class' => '',
					'category' => esc_html__('Interrio', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show Services Single 2.', BUNCH_NAME),
					'params' => array(
						array(
							'type' => 'group',
							'label' => esc_html__( 'Services Single Details', BUNCH_NAME ),
							'name' => 'services_single_2',
							'description' => esc_html__( 'Enter Services Single detail.', BUNCH_NAME ),
							'params' => array(
								$title,
								$img,
								$icon,
								$url,
								$text,
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Pricing', BUNCH_NAME ),
									"name"			=>	"pricing",
									'admin_label' 	=> 	false,
									"description"	=>	esc_html__('Enter pricing in Sq.ft.', BUNCH_NAME )
								),
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Features', BUNCH_NAME ),
									"name"			=>	"features",
									'admin_label' 	=> 	false,
									"description"	=>	esc_html__('Enter Features.', BUNCH_NAME )
								),
							),
						),
						
					),
			);
//Commercial Projects
$options['bunch_commercial_projects'] = array(
					'name' => esc_html__('Commercial Projects', BUNCH_NAME),
					'base' => 'bunch_commercial_projects',
					'class' => '',
					'category' => esc_html__('Interrio', BUNCH_NAME),
					'icon' => 'fa-th-large' ,
					'description' => esc_html__('Show Commercial Projects.', BUNCH_NAME),
					'params' => array(
						$editor,
						$number,
						array(
						   "type" => "textfield",
						   "label" => __('Excluded Categories ID', BUNCH_NAME ),
						   "name" => "exclude_cats",
						   "description" => __('Enter Excluded Categories ID seperated by commas(13,14).', BUNCH_NAME )
						),
						$orderby,
						$order,
					),
			);
//Commercial Appoinment
$options['bunch_commercial_appoinment']	=	array(
					'name' => esc_html__('Commercial Appoinment', BUNCH_NAME),
					'base' => 'bunch_commercial_appoinment',
					'class' => '',
					'category' => esc_html__('Interrio', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show Commercial Appoinment.', BUNCH_NAME),
					'params' => array(
						esc_html__( 'General', BUNCH_NAME ) => array(
							$bg_img,
							$editor,
							$contact_text,
						),
						esc_html__( 'Business Hours', BUNCH_NAME ) => array(
							$title,
							array(
								'type' => 'text',
								'label' => esc_html__( 'Monday - Friday', BUNCH_NAME ),
								'name' => 'monday_friday',
								'description' => esc_html__( 'Enter Weekdays time.', BUNCH_NAME ),
							),
							array(
								'type' => 'text',
								'label' => esc_html__( 'Saturday', BUNCH_NAME ),
								'name' => 'saturday',
								'description' => esc_html__( 'Enter Saturday time.', BUNCH_NAME ),
							),
							array(
								'type' => 'text',
								'label' => esc_html__( 'Sunday', BUNCH_NAME ),
								'name' => 'sunday',
								'description' => esc_html__( 'Enter Sunday time.', BUNCH_NAME ),
							),
							$text,
						),
					),
			);
// Price Plan
$options['bunch_price_plan'] = array(
					'name' => esc_html__('Price Plan', BUNCH_NAME),
					'base' => 'bunch_price_plan',
					'class' => '',
					'category' => esc_html__('Interrio', BUNCH_NAME),
					'icon' => 'fa-usd' ,
					'description' => esc_html__('Show Price Plan.', BUNCH_NAME),
					'params' => array(
						$editor,
						array(
							'type' => 'group',
							'label' => esc_html__( 'Price Plan', BUNCH_NAME ),
							'name' => 'pricing_plan',
							'description' => esc_html__( 'Enter Price Plan detail.', BUNCH_NAME ),
							'params' => array(
								array(
									'type' => 'text',
									'label' => esc_html__( 'Price Plan Title', BUNCH_NAME ),
									'name' => 'price_title',
									'description' => esc_html__( 'Enter Price Plan Title.', BUNCH_NAME ),
								),
								array(
									'type' => 'text',
									'label' => esc_html__( 'Currency Sign', BUNCH_NAME ),
									'name' => 'sign',
									'description' => esc_html__( 'Enter Currency Sign.', BUNCH_NAME ),
									'value' => '$'
								),
								array(
									'type' => 'text',
									'label' => esc_html__( 'Amount', BUNCH_NAME ),
									'name' => 'amount',
									'description' => esc_html__( 'Enter Name.', BUNCH_NAME ),
								),
								array(
									'type' => 'text',
									'label' => esc_html__( 'Month', BUNCH_NAME ),
									'name' => 'month',
									'description' => esc_html__( 'Enter Description.', BUNCH_NAME ),
									'value' => 'Mo'
								),
								$text,
								$btn_title,
								$btn_link,
							),
						),
					),
			);
//Customer Feedback 2
$options['bunch_customer_feedback_2'] = array(
					'name' => esc_html__('Customer Feedback 2', BUNCH_NAME),
					'base' => 'bunch_customer_feedback_2',
					'class' => '',
					'category' => esc_html__('Interrio', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show Customer Feedback on Services Single.', BUNCH_NAME),
					'params' => array(
						$bg_img,
						$title,
						$text_limit,
						$number,
						array(
							"type" => "dropdown",
							"label" => __('Category', BUNCH_NAME),
							"name" => "cat",
							"options" =>  bunch_get_categories(array( 'taxonomy' => 'testimonials_category'), true),
							"description" => __( 'Choose Category.', BUNCH_NAME)
						),
						$orderby,
						$order,
					),
			);
//About Us
$options['bunch_about_us']	=	array(
					'name' => esc_html__('About Us', BUNCH_NAME),
					'base' => 'bunch_about_us',
					'class' => '',
					'category' => esc_html__('Interrio', BUNCH_NAME),
					'icon' => 'fa-cog' ,
					'description' => esc_html__('Show About Us.', BUNCH_NAME),
					'is_container' =>true,
					'params' => array(
						$editor,
						array(
							"type"			=>	"textarea",
							"label"			=>	esc_html__('Sub Title', BUNCH_NAME ),
							"name"			=>	"sub_title",
							"description"	=>	esc_html__('Enter sub title to show.', BUNCH_NAME )
						),
						$text,
						array(
							"type"			=>	"text",
							"label"			=>	esc_html__('Name', BUNCH_NAME ),
							"name"			=>	"name",
							"description"	=>	esc_html__('Enter Name.', BUNCH_NAME )
						),
						array(
							"type"			=>	"text",
							"label"			=>	esc_html__('Designation', BUNCH_NAME ),
							"name"			=>	"designation",
							"description"	=>	esc_html__('Enter Designation.', BUNCH_NAME )
						),
						$signature,
						array(
							'type' => 'group',
							'label' => esc_html__( 'Our Mission & Vision', BUNCH_NAME ),
							'name' => 'mission_vision',
							'description' => esc_html__( 'Enter Our Mission & Vision.', BUNCH_NAME ),
							'params' => array(
								$title,
								$text,
								$img,
								$url,
							),
						),
					),
			);
//Why Choose Interior
$options['bunch_why_choose_interior']	=	array(
					'name' => esc_html__('Why Choose Interior', BUNCH_NAME),
					'base' => 'why_choose_interior',
					'class' => '',
					'category' => esc_html__('Interrio', BUNCH_NAME),
					'icon' => 'fa-cog' ,
					'description' => esc_html__('Show Why Choose Interior.', BUNCH_NAME),
					'params' => array(
						$title,
						$bg_img,
						array(
							'type' => 'group',
							'label' => esc_html__( 'Why Choose Interior Detail', BUNCH_NAME ),
							'name' => 'choose_interior',
							'description' => esc_html__( 'Enter Why Choose Interior Details.', BUNCH_NAME ),
							'params' => array(
								$icon,
								$title,
								$text,
							),
						),
					),
			);
//Meet The Team
$options['bunch_meet_the_team'] = array(
					'name' => esc_html__('Meet The Team', BUNCH_NAME),
					'base' => 'bunch_meet_the_team',
					'class' => '',
					'category' => esc_html__('Interrio', BUNCH_NAME),
					'icon' => 'fa-briefcase',
					'description' => esc_html__('Show Meet The Team.', BUNCH_NAME),
					'params' => array(
						$editor,
						$number,
						array(
							"type" => "dropdown",
							"label" => __('Category', BUNCH_NAME),
							"name" => "cat",
							"options" =>  bunch_get_categories(array( 'taxonomy' => 'team_category'), true),
							"description" => __( 'Choose Category.', BUNCH_NAME)
						),
						$orderby,
						$order,
						$text,
					),
			);
//Call Out About
$options['bunch_call_out_about']	=	array(
					'name' => esc_html__('Call Out About Us', BUNCH_NAME),
					'base' => 'bunch_call_out_about',
					'class' => '',
					'category' => esc_html__('Interrio', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show Call Out About us.', BUNCH_NAME),
					'params' => array(
						$bg_img,
						$text,
					),
			);
//Interesting Facts
$options['bunch_fun_facts']	=	array(
					'name' => esc_html__('Interesting Facts', BUNCH_NAME),
					'base' => 'bunch_fun_facts',
					'class' => '',
					'category' => esc_html__('Interrio', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show Fun Facts.', BUNCH_NAME),
					'params' => array(
						esc_html__( 'General', BUNCH_NAME ) => array(
							array(
								"type"			=>	"text",
								"label"			=>	esc_html__('Title', BUNCH_NAME ),
								"name"			=>	"title",
								'admin_label' 	=> 	false,
								"description"	=>	esc_html__('Choose Title.', BUNCH_NAME )
							),
						),
						esc_html__( 'Interesting Facts', BUNCH_NAME ) => array(
							array(
								'type' => 'group',
								'label' => esc_html__( 'Interesting Facts Slides', BUNCH_NAME ),
								'name' => 'interesting_facts',
								'description' => esc_html__( 'Enter Interesting Facts Details.', BUNCH_NAME ),
								'params' => array(
									$text,
								),
							),
						),
						esc_html__( 'Fun Facts', BUNCH_NAME ) => array(
							array(
								'type' => 'group',
								'label' => esc_html__( 'Fun Facts', BUNCH_NAME ),
								'name' => 'fun_facts',
								'description' => esc_html__( 'Enter Fun Facts Details.', BUNCH_NAME ),
								'params' => array(
									$title,
									$icon,
									$counter_start,
									$counter_end,
									$counter_sign,
								),
							),
						),
					),
			);
//Press Release
$options['bunch_press_release']	=	array(
					'name' => esc_html__('Press Release', BUNCH_NAME),
					'base' => 'bunch_press_release',
					'class' => '',
					'category' => esc_html__('Interrio', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show Press Release.', BUNCH_NAME),
					'params' => array(
						esc_html__( 'General', BUNCH_NAME ) => array(
							$img,
							$title,
							$text,
							array(
								"type"			=>	"text",
								"label"			=>	esc_html__('Name', BUNCH_NAME ),
								"name"			=>	"name",
								"description"	=>	esc_html__('Enter Name.', BUNCH_NAME )
							),
							array(
								"type"			=>	"text",
								"label"			=>	esc_html__('Designation', BUNCH_NAME ),
								"name"			=>	"designation",
								"description"	=>	esc_html__('Enter Designation.', BUNCH_NAME )
							),
							$signature,
						),
						esc_html__( 'Press Release', BUNCH_NAME ) => array(
							array(
								'type' => 'group',
								'label' => esc_html__( 'Press Release Posts', BUNCH_NAME ),
								'name' => 'press_release',
								'description' => esc_html__( 'Enter Press Release Details.', BUNCH_NAME ),
								'params' => array(
									$title,
									$img,
									$url,
									$date,
								),
							),
						),
					),
			);
//Testimonials
$options['bunch_testimonials'] = array(
					'name' => esc_html__('Testimonials', BUNCH_NAME),
					'base' => 'bunch_testimonials',
					'class' => '',
					'category' => esc_html__('Interrio', BUNCH_NAME),
					'icon' => 'fa-briefcase',
					'description' => esc_html__('Show Testimonials.', BUNCH_NAME),
					'params' => array(
						$text_limit,
						$number,
						array(
							"type" => "dropdown",
							"label" => __('Category', BUNCH_NAME),
							"name" => "cat",
							"options" =>  bunch_get_categories(array( 'taxonomy' => 'testimonials_category'), true),
							"description" => __( 'Choose Category.', BUNCH_NAME)
						),
						$orderby,
						$order,
					),
			);
// FAQs Start.
$options['bunch_faqs']	=	array(
					'name' => esc_html__('FAQs - Contact Form', BUNCH_NAME),
					'base' => 'bunch_faqs',
					'class' => '',
					'category' => esc_html__('Interrio', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show FAQs.', BUNCH_NAME),
					'params' => array(
						esc_html__( 'FAQs', BUNCH_NAME ) => array(
							array(
								"type" => "dropdown",
								"label" => __( 'Category', BUNCH_NAME),
								"name" => "cat",
								"options" =>  bunch_get_categories(array( 'taxonomy' => 'faqs_category'), true),
								"description" => __( 'Choose Category.', BUNCH_NAME)
							),
							$number,
							$orderby,
							$order,
						),
						esc_html__( 'Contact Form', BUNCH_NAME ) => array(
							$title,
							$text,
						),
					),
			);
//Projects Grid 2 Columns
$options['bunch_project_grid_2_col'] = array(
					'name' => esc_html__('Projects Grid 2 Column', BUNCH_NAME),
					'base' => 'bunch_project_grid_2_col',
					'class' => '',
					'category' => esc_html__('Interrio', BUNCH_NAME),
					'icon' => 'fa-th-large' ,
					'description' => esc_html__('Show projects grid 2 column.', BUNCH_NAME),
					'params' => array(
						$number,
						array(
						   "type" => "textfield",
						   "label" => __('Excluded Categories ID', BUNCH_NAME ),
						   "name" => "exclude_cats",
						   "description" => __('Enter Excluded Categories ID seperated by commas(13,14).', BUNCH_NAME )
						),
						$orderby,
						$order,
					),
			);
//Projects Grid 3 Columns
$options['bunch_project_grid_3_col'] = array(
					'name' => esc_html__('Projects Grid 3 Column', BUNCH_NAME),
					'base' => 'bunch_project_grid_3_col',
					'class' => '',
					'category' => esc_html__('Interrio', BUNCH_NAME),
					'icon' => 'fa-th-large' ,
					'description' => esc_html__('Show projects grid 3 column.', BUNCH_NAME),
					'params' => array(
						$number,
						array(
						   "type" => "textfield",
						   "label" => __('Excluded Categories ID', BUNCH_NAME ),
						   "name" => "exclude_cats",
						   "description" => __('Enter Excluded Categories ID seperated by commas(13,14).', BUNCH_NAME )
						),
						$orderby,
						$order,
					),
			);
//Projects Grid 4 Columns
$options['bunch_project_grid_4_col'] = array(
					'name' => esc_html__('Projects Grid 4 Column', BUNCH_NAME),
					'base' => 'bunch_project_grid_4_col',
					'class' => '',
					'category' => esc_html__('Interrio', BUNCH_NAME),
					'icon' => 'fa-th-large' ,
					'description' => esc_html__('Show projects grid 4 column.', BUNCH_NAME),
					'params' => array(
						$number,
						array(
						   "type" => "textfield",
						   "label" => __('Excluded Categories ID', BUNCH_NAME ),
						   "name" => "exclude_cats",
						   "description" => __('Enter Excluded Categories ID seperated by commas(13,14).', BUNCH_NAME )
						),
						$orderby,
						$order,
					),
			);
//Projects Classic 2 Columns
$options['bunch_project_classic_2_col'] = array(
					'name' => esc_html__('Projects Classic 2 Column', BUNCH_NAME),
					'base' => 'bunch_project_classic_2_col',
					'class' => '',
					'category' => esc_html__('Interrio', BUNCH_NAME),
					'icon' => 'fa-th-large' ,
					'description' => esc_html__('Show projects classic 2 column.', BUNCH_NAME),
					'params' => array(
						$number,
						array(
						   "type" => "textfield",
						   "label" => __('Excluded Categories ID', BUNCH_NAME ),
						   "name" => "exclude_cats",
						   "description" => __('Enter Excluded Categories ID seperated by commas(13,14).', BUNCH_NAME )
						),
						$orderby,
						$order,
					),
			);
//Projects Classic 3 Columns
$options['bunch_project_classic_3_col'] = array(
					'name' => esc_html__('Projects Classic 3 Column', BUNCH_NAME),
					'base' => 'bunch_project_classic_3_col',
					'class' => '',
					'category' => esc_html__('Interrio', BUNCH_NAME),
					'icon' => 'fa-th-large' ,
					'description' => esc_html__('Show projects classic 3 column.', BUNCH_NAME),
					'params' => array(
						$number,
						array(
						   "type" => "textfield",
						   "label" => __('Excluded Categories ID', BUNCH_NAME ),
						   "name" => "exclude_cats",
						   "description" => __('Enter Excluded Categories ID seperated by commas(13,14).', BUNCH_NAME )
						),
						$orderby,
						$order,
					),
			);
//Projects Classic 4 Columns
$options['bunch_project_classic_4_col'] = array(
					'name' => esc_html__('Projects Classic 4 Column', BUNCH_NAME),
					'base' => 'bunch_project_classic_4_col',
					'class' => '',
					'category' => esc_html__('Interrio', BUNCH_NAME),
					'icon' => 'fa-th-large' ,
					'description' => esc_html__('Show projects classic 4 column.', BUNCH_NAME),
					'params' => array(
						$number,
						array(
						   "type" => "textfield",
						   "label" => __('Excluded Categories ID', BUNCH_NAME ),
						   "name" => "exclude_cats",
						   "description" => __('Enter Excluded Categories ID seperated by commas(13,14).', BUNCH_NAME )
						),
						$orderby,
						$order,
					),
			);
//Projects With Text
$options['bunch_project_with_text'] = array(
					'name' => esc_html__('Projects With Text', BUNCH_NAME),
					'base' => 'bunch_project_with_text',
					'class' => '',
					'category' => esc_html__('Interrio', BUNCH_NAME),
					'icon' => 'fa-th-large' ,
					'description' => esc_html__('Show Projects With Text.', BUNCH_NAME),
					'params' => array(
						$number,
						array(
						   "type" => "textfield",
						   "label" => __('Excluded Categories ID', BUNCH_NAME ),
						   "name" => "exclude_cats",
						   "description" => __('Enter Excluded Categories ID seperated by commas(13,14).', BUNCH_NAME )
						),
						$orderby,
						$order,
					),
			);
//Projects Full Width
$options['bunch_project_fullwidth'] = array(
					'name' => esc_html__('Projects Full Width', BUNCH_NAME),
					'base' => 'bunch_project_fullwidth',
					'class' => '',
					'category' => esc_html__('Interrio', BUNCH_NAME),
					'icon' => 'fa-th-large' ,
					'description' => esc_html__('Show Full Width', BUNCH_NAME),
					'params' => array(
						$number,
						array(
						   "type" => "textfield",
						   "label" => __('Excluded Categories ID', BUNCH_NAME ),
						   "name" => "exclude_cats",
						   "description" => __('Enter Excluded Categories ID seperated by commas(13,14).', BUNCH_NAME )
						),
						$orderby,
						$order,
					),
			);
//Projects Masonry v1
$options['bunch_projects_masonry_v1'] = array(
					'name' => esc_html__('Projects Masonry V1', BUNCH_NAME),
					'base' => 'bunch_projects_masonry_v1',
					'class' => '',
					'category' => esc_html__('Interrio', BUNCH_NAME),
					'icon' => 'fa-th-large' ,
					'description' => esc_html__('Show Projects Masonry V1.', BUNCH_NAME),
					'params' => array(
						$number,
						array(
						   "type" => "textfield",
						   "label" => __('Excluded Categories ID', BUNCH_NAME ),
						   "name" => "exclude_cats",
						   "description" => __('Enter Excluded Categories ID seperated by commas(13,14).', BUNCH_NAME )
						),
						$orderby,
						$order,
					),
			);
//Projects Masonry v12
$options['bunch_projects_masonry_v2'] = array(
					'name' => esc_html__('Projects Masonry V2', BUNCH_NAME),
					'base' => 'bunch_projects_masonry_v2',
					'class' => '',
					'category' => esc_html__('Interrio', BUNCH_NAME),
					'icon' => 'fa-th-large' ,
					'description' => esc_html__('Show Projects Masonry V2', BUNCH_NAME),
					'params' => array(
						$number,
						array(
						   "type" => "textfield",
						   "label" => __('Excluded Categories ID', BUNCH_NAME ),
						   "name" => "exclude_cats",
						   "description" => __('Enter Excluded Categories ID seperated by commas(13,14).', BUNCH_NAME )
						),
						$orderby,
						$order,
					),
			);
//Blog Standard
$options['bunch_blog_standard'] = array(
					'name' => esc_html__('Blog Standard', BUNCH_NAME),
					'base' => 'bunch_blog_standard',
					'class' => '',
					'category' => esc_html__('Interrio', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show Blog Standard.', BUNCH_NAME),
					'params' => array(
						$text_limit,
						$number,
						array(
							"type" => "dropdown",
							"label" => __('Category', BUNCH_NAME),
							"name" => "cat",
							"options" =>  bunch_get_categories(array('taxonomy' => 'category'), true),
							"description" => __('Choose Category.', BUNCH_NAME)
						),
						$orderby,
						$order,
					),
			);
// Contact Us 01
$options['bunch_contact_us_v1']	=	array(
					'name' => esc_html__('Contact Us V1', BUNCH_NAME),
					'base' => 'contact_us_v1',
					'class' => '',
					'category' => esc_html__('Interrio', BUNCH_NAME),
					'icon' => 'fa-envelope' ,
					'description' => esc_html__('Show contact detail.', BUNCH_NAME),
					'params' => array(
						$title,
						$text,
						$img,
						$address,
						$phone,
						$email,
						$working_hours,
						$contact_text,
					),
			);
// Contact Us 02
$options['bunch_contact_us_v2']	=	array(
					'name' => esc_html__('Contact Us V2', BUNCH_NAME),
					'base' => 'bunch_contact_us_v2',
					'class' => '',
					'category' => esc_html__('Interrio', BUNCH_NAME),
					'icon' => 'fa-envelope' ,
					'description' => esc_html__('Show contact detail.', BUNCH_NAME),
					'params' => array(
						esc_html__( 'General', BUNCH_NAME ) => array(
							array(
								'type' => 'group',
								'label' => esc_html__( 'Office Address', BUNCH_NAME ),
								'name' => 'office_address',
								'description' => esc_html__( 'Enter Office Address.', BUNCH_NAME ),
								'params' => array(
									$title,
									$address,
									$phone,
									$email,
									$working_hours,
								),
							),
						),
						esc_html__( 'Google Map', BUNCH_NAME ) => array(
							array(
								"type"			=>	"text",
								"label"			=>	esc_html__('Office Name', BUNCH_NAME ),
								"name"			=>	"office_name",
								"description"	=>	esc_html__('Enter Office Name to show.', BUNCH_NAME )
							),
							$address,
							$latitude,
							$longitude,
							$zoom,
						),
						esc_html__( 'Contact Form', BUNCH_NAME ) => array(
							$contact_title,
							$text,
							$contact_text,
						),
					),
			);
// Google Map
$options['bunch_google_map']	=	array(
					'name' => esc_html__('Google Map', BUNCH_NAME),
					'base' => 'bunch_google_map',
					'class' => '',
					'category' => esc_html__('Interrio', BUNCH_NAME),
					'icon' => 'fa-envelope' ,
					'description' => esc_html__('Show Google Map.', BUNCH_NAME),
					'params' => array(
						$address,
						$latitude,
						$longitude,
						$zoom,
						array(
							'type' => 'group',
							'label' => esc_html__( 'Office Address', BUNCH_NAME ),
							'name' => 'office_address',
							'description' => esc_html__( 'Enter Office Address Details.', BUNCH_NAME ),
							'params' => array(
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Office Name', BUNCH_NAME ),
									"name"			=>	"text",
									"description"	=>	esc_html__('Enter Office Name to show.', BUNCH_NAME )
								),
								$address,
								$latitude,
								$longitude,
							),
						),
					),
			);
			
			












/************************************************************************************************************/
// Service box start.
$options['bunch_services_box']	=	array(
					'name' => esc_html__('Services Box', BUNCH_NAME),
					'base' => 'bunch_services_box',
					'class' => '',
					'category' => esc_html__(BUNCH_NAME, BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show  services.', BUNCH_NAME),
									'params' => array(
										array(
											'type'            => 'group',
											'label'            => __('Services', BUNCH_NAME),
											'name'            => 'services',
											'description'    => esc_html__('Show our services.', BUNCH_NAME),
											'options'        => array('add_text' => __('Add our Service', BUNCH_NAME)),
										
									
											'params' => array(
													
												array(
													'type' => 'icon_picker',
													'label' => esc_html__('Icon', BUNCH_NAME ),
													'name' => 'icon',
													'description' => esc_html__('Enter your icon', BUNCH_NAME )
												),
												array(
													'type' => 'text',
													'label' => esc_html__('Title', BUNCH_NAME ),
													'name' => 'title',
													'description' => esc_html__('Enter our services title', BUNCH_NAME )
												),
												array(
													'type' => 'link',
													'label' => esc_html__(' Link text', BUNCH_NAME ),
													'name' => 'link',
													'description' => esc_html__('Enter Readmore link', BUNCH_NAME )
												),
											
									
												array(
													'type' => 'textarea',
													'label' => esc_html__('Content', BUNCH_NAME ),
													'name' => 'text',
													'description' => esc_html__('Enter the services text', BUNCH_NAME )
												),
											)																				    		)
							            ),
		            	            );
					
//services_box end 
//Around the university start 
			
$options['bunch_around_the_university']	=	array(
					'name' => esc_html__('around the university', BUNCH_NAME),
					'base' => 'bunch_around_the_university',
					'class' => '',
					'category' => esc_html__(BUNCH_NAME, BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show  around the university.', BUNCH_NAME),
									'params' => array(
										array(
											'type' => 'text',
											'label' => esc_html__('Title', BUNCH_NAME ),
											'name' => 'title',
											'description' => esc_html__( 'Enter the section title', BUNCH_NAME )
										),
										array(
												'type' => 'text',
												'label' => esc_html__( 'Tag line', BUNCH_NAME ),
												'name' => 'tagline',
												'description' => esc_html__( 'Enter our tag line', BUNCH_NAME ),
										),
										
										array(
											'type'            => 'group',
											'label'            => __('Services', BUNCH_NAME),
											'name'            => 'services',
											'description'    => esc_html__('Show our services.', BUNCH_NAME),
											'options'        => array('add_text' => __('Add our Service', BUNCH_NAME)),
										
											'params' => array(
												
													array(
														'type' => 'attach_image',
														'label' => esc_html__('Image', BUNCH_NAME ),
														'name' => 'image',
														'description' => esc_html__('Enter your icon', BUNCH_NAME ),
													),
													array(
														'type' => 'text',
														'label' => esc_html__('Title', BUNCH_NAME ),
														'name' => 'title',
														'description' => esc_html__('Enter our services title', BUNCH_NAME )
													),
													array(
														'type' => 'link',
														'label' => esc_html__('Readmore Link', BUNCH_NAME ),
														'name' => 'link',
														'description' => esc_html__('Enter Readmore link', BUNCH_NAME )
													),
										  
										    
											    	array(
														'type' => 'textarea',
														'label' => esc_html__('text ', BUNCH_NAME ),
														'name' => 'text',
														'description' => esc_html__('Enter your left text', BUNCH_NAME )
													),
											)
											)
										),
									);


// Around the university end.
// Parallax start.
$options['bunch_parallaxx']	= array(
					'name' => esc_html__( 'Parallax', BUNCH_NAME ),
					'base' => 'bunch_parallax',
					'class' => '',
					'category' => esc_html__( BUNCH_NAME, BUNCH_NAME ),
					'icon' => 'fa-briefcase',
					'description' => esc_html__( 'Show  parallax.', BUNCH_NAME ),
					'params' => array(
			                 array(
								'type' => 'attach_image',
								'label' => esc_html__( 'Image', BUNCH_NAME ),
								'name' => 'bg_image',
								'description' => esc_html__( 'select image', BUNCH_NAME ),
							 ),
							 array(
								'type' => 'icon_picker',
								'label' => esc_html__( 'Icon', BUNCH_NAME ),
								'name' => 'icon',
								'admin_label' => true,
								'description' => esc_html__( 'Enter your icon', BUNCH_NAME ),
							),
							array(
								'type' => 'text',
								'label' => esc_html__( 'text left', BUNCH_NAME ),
								'name' => 'text_left',
								'description' => esc_html__( 'Enter your left text', BUNCH_NAME ),
							),
							array(
								'type' => 'text',
								'label' => esc_html__( 'text right', BUNCH_NAME ),
								'name' => 'text_right',
								'description' => esc_html__( 'Enter your right text', BUNCH_NAME ),
							),
							array(
								'type' => 'link',
								'label' => esc_html__( 'Icon Link', BUNCH_NAME ),
								'name' => 'link',
								'description' => esc_html__( 'Enter Readmore link', BUNCH_NAME ),
							),

						),
					);
// END OF PARALLAX.
// START OF POLLAR COURSE.
$options['bunch_popular_course']	=	array(
				'name' => esc_html__( 'Popular Courses', BUNCH_NAME),
				'base' => 'bunch_popular_course',
				'class' => '',
				'category' => esc_html__( BUNCH_NAME, BUNCH_NAME ),
				'icon' => 'fa-briefcase',
				'description' => esc_html__( 'Show our services.', BUNCH_NAME ),
				'params' => array(
								array(
									'type' => 'text',
									'label' => esc_html__( 'Title', BUNCH_NAME ),
									'name' => 'title',
									'description' => esc_html__( 'Enter title', BUNCH_NAME ),
								),
								array(
									'type' => 'text',
									'label' => esc_html__( 'Tagline', BUNCH_NAME ),
									'name' => 'tagline',
									'description' => esc_html__( 'Enter tagline', BUNCH_NAME ),
								),
								array(
									'type' => 'text',
									'label' => esc_html__( 'Number of Courses', BUNCH_NAME ),
									'name' => 'num',
									'description' => esc_html__( 'Number of courses to show', BUNCH_NAME ),
								),
								array(
									'type' => 'autocomplete',
									'label' => esc_html__( 'Category', BUNCH_NAME ),
									'name' => 'cat',
									'options' => array(
													'taxonomy' => 'product_cat',
									),
									'description' => esc_html__( 'Choose the category.', BUNCH_NAME ),
								),
								array(
									'type' => 'autocomplete',
									'label' => esc_html__( 'Category', BUNCH_NAME ),
									'name' => 'tag',
									'options' => array(
													'taxonomy' => 'product_tag',
									),
									'description' => esc_html__( 'Choose the Tags.', BUNCH_NAME ),
								),

								// Order by array mentioned at the top of file.
								$orderby,

								// Order array either asceding or descending.
								

								array(
								   'type' => 'dropdown',
								   'label' => esc_html__( 'Columns', BUNCH_NAME ),
								   'name' => 'columns',
								   'options'		=> array(
								   							'one'	=> esc_html__( 'One Column', BUNCH_NAME ),
								   							'two'	=> esc_html__( 'Two Column', BUNCH_NAME ),
								   							'three'	=> esc_html__( 'Three Column', BUNCH_NAME ),
								   							'four'	=> esc_html__( 'Four Column', BUNCH_NAME ),
								   	),
								   'description' => esc_html__( 'Choose number of columns.', BUNCH_NAME ),
								),
								array(
									'type' => 'toggle',
									'label' => esc_html__( 'Enable Carousel', BUNCH_NAME ),
									'name' => 'carousel',
									'description' => esc_html__( 'Enable to show courses in carousel.', BUNCH_NAME ),
								),
								array(
									'type' => 'toggle',
									'label' => esc_html__( 'Hide Price', BUNCH_NAME ),
									'name' => 'hide_price',
									'description' => esc_html__( 'Hide price.', BUNCH_NAME ),
								),
								array(
									'type' => 'toggle',
									'label' => esc_html__( 'Hide Instructor', BUNCH_NAME ),
									'name' => 'hide_inst',
									'description' => esc_html__( 'Hide instructor.', BUNCH_NAME ),
								),
								array(
									'type' => 'toggle',
									'label' => esc_html__( 'Hide Rating', BUNCH_NAME ),
									'name' => 'hide_rating',
									'description' => esc_html__( 'Hide Rating.', BUNCH_NAME ),
								),
								array(
									'type' => 'toggle',
									'label' => esc_html__( 'Hide Date', BUNCH_NAME ),
									'name' => 'hide_date',
									'description' => esc_html__( 'Hide Date.', BUNCH_NAME ),
								),
								array(
									'type' => 'link',
									'label' => esc_html__( 'Course Page Link', BUNCH_NAME ),
									'name' => 'courses_link',
									'description' => esc_html__( 'Choose the courses page link.', BUNCH_NAME ),
								),
								array(
								   'type' => 'color_picker',
								   'label' => esc_html__( 'Select background color', BUNCH_NAME ),
								   'name' => 'bg_color',
								   'description' => esc_html__( 'Choose Slider.', BUNCH_NAME ),
								),

				),
		);

return $options;