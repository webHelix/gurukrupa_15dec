<?php $options = _WSH()->option();
	get_header(); 
	$settings  = interrio_set(interrio_set(get_post_meta(get_the_ID(), 'bunch_page_meta', true) , 'bunch_page_options') , 0);
	$meta = _WSH()->get_meta('_bunch_layout_settings');
	$meta1 = _WSH()->get_meta('_bunch_header_settings');
	$meta2 = _WSH()->get_meta();
	_WSH()->page_settings = $meta;
	
	if(interrio_set($_GET, 'layout_style'))
	$layout = interrio_set($_GET, 'layout_style');
	else
	$layout = interrio_set( $meta, 'layout', 'right' );
	
	if( !$layout || $layout == 'full' || interrio_set($_GET, 'layout_style')=='full' )
	$sidebar = '';
	else
	$sidebar = interrio_set( $meta, 'sidebar', 'blog-sidebar' );
	
	$layout = ($layout) ? $layout : 'right';
	$sidebar = ($sidebar) ? $sidebar : 'blog-sidebar';
	
	$classes = ( !$layout || $layout == 'full' || interrio_set($_GET, 'layout_style')=='full' ) ? ' col-lg-12 col-md-12 col-sm-12 col-xs-12 ' : ' col-lg-9 col-md-8 col-sm-12 col-xs-12 ' ;
	/** Update the post views counter */
	_WSH()->post_views( true );
	$bg = interrio_set($meta1, 'header_img');
	$title = interrio_set($meta1, 'header_title');
?>
<!--Start breadcrumb area-->     
<section class="breadcrumb-area" <?php if($bg):?>style="background-image:url('<?php echo esc_url($bg)?>');"<?php endif;?>>
	<div class="container-fluid text-center">
		<h1><?php if($title) echo wp_kses_post($title); else wp_title(''); ?></h1>
		<div class="breadcrumb-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="left pull-left">
                            <?php echo balanceTags(interrio_get_the_breadcrumb()); ?>
                        </div>
                        <?php if(interrio_set($meta, 'quote_link')){ ?>
                        <div class="right pull-right">
<?php // echo esc_url(interrio_set($meta, 'quote_link')); ?>
                            <a href="http://httest.in/gurukrupa/guru/contact-us/"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i><?php esc_html_e('Get a Quote', 'interrio'); ?>jij jiuji</a>
                        </div> 
                        <?php } ?>   
                    </div>
                </div>
            </div>
		</div>
	</div>
</section>
<!--End breadcrumb area-->

<!--Sidebar Page-->
<section id="blog-area" class="blog-single-area">
    <div class="container">
        <div class="row">
            
            <!-- sidebar area -->
			<?php if( $layout == 'left' ): ?>
			<?php if ( is_active_sidebar( $sidebar ) ) { ?>
			<div class="col-lg-3 col-md-4 col-sm-7 col-xs-12">        
				<aside class="sidebar-wrapper">
					<?php dynamic_sidebar( $sidebar ); ?>
				</aside>
            </div>
			<?php } ?>
			<?php endif; ?>
            
            <!--Content Side-->	
            <div class="<?php echo esc_attr($classes);?>">
                
                <!--Default Section-->
                <section class="default-section blog-section no-padd-top no-padd-bottom">
                    <div class="thm-unit-test single-tut">
					<?php while( have_posts() ): the_post(); 
						$post_meta = _WSH()->get_meta();
					?>
                    <!--Blog Post-->
                    <div class="blog-post">
                        <!--Start single blog post-->
                        <div class="single-blog-item">
                            <div class="img-holder">
                                <?php the_post_thumbnail('interrio_870x485');?>
                            </div>
                            <div class="text-holder">
                                <ul class="meta-info">
                                    <li><i class="fa fa-user" aria-hidden="true"></i><a href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) )); ?>"><?php the_author(); ?></a></li>
                                    <li><i class="fa fa-clock-o" aria-hidden="true"></i><?php echo get_the_date('F d, Y'); ?></li>
                                    <li><i class="fa fa-folder-open-o" aria-hidden="true"></i><?php the_category(', '); ?></li>
                                    <li><i class="fa fa-comments" aria-hidden="true"></i><a href="<?php echo esc_url(get_permalink(get_the_id()).'#comments');?>"> <?php comments_number( '0 comment', '1 comment', '% comments' ); ?></a></li>
                                </ul>
                                <?php the_content(); ?>
                                <br>
                                <?php wp_link_pages(array('before'=>'<div class="paginate-links">'.esc_html__('Pages: ', 'interrio'), 'after' => '</div>', 'link_before'=>'<span>', 'link_after'=>'</span>')); ?>
                            </div>    
                        </div>
                        
                        <!--Start tag and social share box-->
                        <div class="tag-social-share-box">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="tag pull-left">
                                        <p><?php the_tags(); ?></p>
                                    </div>
                                    <div class="social-share pull-right">
                                        <h5><?php esc_html_e('Share', 'interrio'); ?><i class="fa fa-share-alt" aria-hidden="true"></i></h5>
                                        <ul class="social-share-links">
                                            <li><a href="http://www.facebook.com/sharer.php?u=<?php echo esc_url(get_permalink(get_the_id())); ?>"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="https://twitter.com/share?url=<?php echo esc_url(get_permalink(get_the_id())); ?>&text=<?php echo esc_attr($post_slug=$post->post_name); ?>"><i class="fa fa-twitter"></i></a></li>
                                            <li><a href="https://plus.google.com/share?url=<?php echo esc_url(get_permalink(get_the_id())); ?>"><i class="fa fa-google-plus"></i></a></li>
                                            <li><a href="http://www.linkedin.com/shareArticle?url=<?php echo esc_url(get_permalink(get_the_id())); ?>&title=<?php echo esc_attr($post_slug=$post->post_name); ?>"><i class="fa fa-linkedin"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--End tag and social share box-->
                        
                        <?php if(get_the_author_meta('description')){ ?>
                        <!--Start author box-->
                        <div class="author-box">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="img-holder">
                                        <?php echo get_avatar(get_the_author_meta( 'ID' ), 85 ); ?>
                                    </div>
                                    <div class="text-holder">
                                        <h3><?php the_author(); ?></h3>
                                        <p><?php the_author_meta( 'description', get_the_author_meta('ID') ); ?></p>
                                        <ul class="social-link">
                                        	<?php if($value = get_the_author_meta('facebook') ): ?>
                                            <li><a href="<?php echo esc_url($value); ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                            <?php endif; ?>
                                            <?php if($value = get_the_author_meta('twitter') ): ?>
                                            <li><a href="<?php echo esc_url($value); ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                            <?php endif; ?>
                                            <?php if($value = get_the_author_meta('google-plus') ): ?>
                                            <li><a href="<?php echo esc_url($value); ?>"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                            <?php endif; ?>
                                            <?php if($value = get_the_author_meta('youtube') ): ?>
                                            <li><a href="<?php echo esc_url($value); ?>"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--End author box-->
                        <?php } ?>
                        
                        <!--Start add comment box-->
                        <?php comments_template(); ?>
                        <!--End add comment box-->
                    </div>
                    <?php endwhile;?>
                    </div>
                </section>
            </div>
            <!--Content Side-->
            
            <!-- sidebar area -->
			<?php if( $layout == 'right' ): ?>
			<?php if ( is_active_sidebar( $sidebar ) ) { ?>
			<div class="col-lg-3 col-md-4 col-sm-7 col-xs-12">        
				<aside class="sidebar-wrapper">
					<?php dynamic_sidebar( $sidebar ); ?>
				</aside>
            </div>
			<?php } ?>
			<?php endif; ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>