<div class="single-blog-item">
    <div class="img-holder">
        <?php the_post_thumbnail('interrio_870x485', array('class' => 'img-responsive')); ?>
        <div class="overlay">
            <div class="box">
                <div class="content">
                    <a href="<?php echo esc_url(get_permalink(get_the_id())); ?>"><i class="fa fa-link"></i></a> 
                </div>
            </div>
        </div>
    </div>
    <div class="text-holder">
        <h3 class="blog-title"><a href="<?php echo esc_url(get_permalink(get_the_id())); ?>"><?php the_title(); ?></a></h3>
        <ul class="meta-info">
            <li><i class="fa fa-user" aria-hidden="true"></i><a href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' )), get_the_author_meta( 'user_nicename' ) ); ?>"><?php the_author(); ?></a></li>
            <li><i class="fa fa-clock-o" aria-hidden="true"></i><?php echo get_the_date('F d, Y'); ?></li>
            <li><i class="fa fa-folder-open-o" aria-hidden="true"></i><?php the_category(', '); ?></li>
            <li><i class="fa fa-comments" aria-hidden="true"></i><a href="<?php echo esc_url(get_permalink(get_the_id()).'#comments'); ?>"><?php comments_number( '0 comment', '1 comment', '% comments' ); ?></a></li>
        </ul>
        <div class="text">
            <?php the_excerpt(); ?>
            <div class="bottom clearfix">
                <div class="readmore pull-left">
                    <a href="<?php echo esc_url(get_permalink(get_the_id())); ?>"><?php esc_html_e('Read More', 'interrio'); ?></a> 
                </div>
                <div class="share pull-right">
                    <h5><?php esc_html_e('Share', 'interrio'); ?><i class="fa fa-share-alt" aria-hidden="true"></i></h5>
                    <ul class="social-share">
                        <li><a href="http://www.facebook.com/sharer.php?u=<?php echo esc_url(get_permalink(get_the_id())); ?>"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://twitter.com/share?url=<?php echo esc_url(get_permalink(get_the_id())); ?>&text=<?php echo esc_attr($post_slug=$post->post_name); ?>"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="https://plus.google.com/share?url=<?php echo esc_url(get_permalink(get_the_id())); ?>"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="http://www.linkedin.com/shareArticle?url=<?php echo esc_url(get_permalink(get_the_id())); ?>&title=<?php echo esc_attr($post_slug=$post->post_name); ?>"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
            </div>
            
        </div>
    </div>    
</div>
