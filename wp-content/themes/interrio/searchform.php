<form class="search-form" action="<?php echo esc_url(home_url('/')); ?>">
    <input placeholder="<?php esc_html_e('Search...', 'interrio'); ?>" type="text" name="s">
    <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
</form>
