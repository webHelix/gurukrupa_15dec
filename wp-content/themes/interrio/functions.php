<?php
add_action('after_setup_theme', 'interrio_bunch_theme_setup');
function interrio_bunch_theme_setup()
{
	global $wp_version;
	if(!defined('INTERRIO_VERSION')) define('INTERRIO_VERSION', '1.0');
	if( !defined( 'INTERRIO_ROOT' ) ) define('INTERRIO_ROOT', get_template_directory().'/');
	if( !defined( 'INTERRIO_URL' ) ) define('INTERRIO_URL', get_template_directory_uri().'/');	
	include_once get_template_directory() . '/includes/loader.php';
	
	
	load_theme_textdomain('interrio', get_template_directory() . '/languages');
	
	//ADD THUMBNAIL SUPPORT
	add_theme_support('post-thumbnails');
	add_theme_support('woocommerce');
	add_theme_support('menus'); //Add menu support
	add_theme_support('automatic-feed-links'); //Enables post and comment RSS feed links to head.
	add_theme_support('widgets'); //Add widgets and sidebar support
	add_theme_support( "title-tag" );
	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );
	/** Register wp_nav_menus */
	if(function_exists('register_nav_menu'))
	{
		register_nav_menus(
			array(
				/** Register Main Menu location header */
				'main_menu' => esc_html__('Main Menu', 'interrio'),
			)
		);
	}
	if ( ! isset( $content_width ) ) $content_width = 960;
	add_image_size( 'interrio_370x180', 370, 180, true ); // '370x180 Our Services'
	add_image_size( 'interrio_443x340', 443, 340, true ); // '443x340 Latest Projects'
	add_image_size( 'interrio_394x264', 394, 264, true ); // '394x264 Customer Feedback'
	add_image_size( 'interrio_350x250', 350, 250, true ); // '350x250 Meet the Team'
	add_image_size( 'interrio_270x170', 270, 170, true ); // '270x170 Testimonials'
	add_image_size( 'interrio_570x416', 570, 416, true ); // '570x416 Projects 2 Col'
	add_image_size( 'interrio_370x270', 370, 270, true ); // '370x270 Projects 3 Col'
	add_image_size( 'interrio_270x195', 270, 195, true ); // '270x195 Projects 4 Col'
	add_image_size( 'interrio_443x340', 443, 340, true ); // '443x340 Projects full width'
	add_image_size( 'interrio_870x485', 870, 485, true ); // '870x485 Our Blog'
	add_image_size( 'interrio_70x80', 70, 80, true ); // '70x80 Recent Posts sidebar'
	add_image_size( 'interrio_80x80', 80, 80, true ); // '80x80 Popular News footer'
	add_image_size( 'interrio_95x95', 95, 95, true ); // '95x95 Customer Feedback services'
	add_image_size( 'interrio_580x500', 580, 500, true ); // '580x500 Masonry Services Single extra width'
	add_image_size( 'interrio_285x500', 285, 500, true ); // '285x500 Masonry Services Single extra height'
	add_image_size( 'interrio_285x245', 285, 245, true ); // '285x245 Masonry Services Single normal'
	add_image_size( 'interrio_770x600', 770, 600, true ); // '770x600 Masonry 1 extra width'
	add_image_size( 'interrio_370x600', 370, 600, true ); // '370x600 Masonry 1 extra height'
	add_image_size( 'interrio_370x260', 370, 260, true ); // '370x260 Masonry 1 normal'
	add_image_size( 'interrio_915x670', 915, 670, true ); // '915x670 Masonry 2 extra width'
	add_image_size( 'interrio_442x710', 442, 710, true ); // '442x710 Masonry 2 extra height'
	add_image_size( 'interrio_443x340', 443, 340, true ); // '443x340 Masonry 2 normal'
	add_image_size( 'interrio_85x85', 85, 85, true ); // '85x85 Latest Projects in footer'
	
}
function interrio_bunch_widget_init()
{
	global $wp_registered_sidebars;
	$theme_options = _WSH()->option();
	if( class_exists( 'Bunch_About_Us' ) )register_widget( 'Bunch_About_Us' );
	if( class_exists( 'Bunch_Recent_Post' ) )register_widget( 'Bunch_Recent_Post' );
	if( class_exists( 'Bunch_Contact_Details' ) )register_widget( 'Bunch_Contact_Details' );
	if( class_exists( 'Bunch_Popular_Post' ) )register_widget( 'Bunch_Popular_Post' );
	if( class_exists( 'Bunch_Newsletter_Projects' ) )register_widget( 'Bunch_Newsletter_Projects' );
	
	
	register_sidebar(array(
	  'name' => esc_html__( 'Default Sidebar', 'interrio' ),
	  'id' => 'default-sidebar',
	  'description' => esc_html__( 'Widgets in this area will be shown on the right-hand side.', 'interrio' ),
	  'before_widget'=>'<div id="%1$s" class="single-sidebar widget %2$s">',
	  'after_widget'=>'</div>',
	  'before_title' => '<div class="sidebar-title"><h1>',
	  'after_title' => '</h1></div>'
	));
	register_sidebar(array(
	  'name' => esc_html__( 'Footer Sidebar', 'interrio' ),
	  'id' => 'footer-sidebar',
	  'description' => esc_html__( 'Widgets in this area will be shown in Footer Area.', 'interrio' ),
	  'before_widget'=>'<div id="%1$s" class="col-md-4 col-sm-6 col-xs-12 column footer-widget %2$s">',
	  'after_widget'=>'</div>',
	  'before_title' => '<div class="title"><h3>',
	  'after_title' => '</h3></div>'
	));
	
	register_sidebar(array(
	  'name' => esc_html__( 'Blog Listing', 'interrio' ),
	  'id' => 'blog-sidebar',
	  'description' => esc_html__( 'Widgets in this area will be shown on the right-hand side.', 'interrio' ),
	  'before_widget'=>'<div id="%1$s" class="single-sidebar widget %2$s">',
	  'after_widget'=>'</div>',
	  'before_title' => '<div class="sidebar-title"><h1>',
	  'after_title' => '</h1></div>'
	));
	if( !is_object( _WSH() )  )  return;
	$sidebars = interrio_set(interrio_set( $theme_options, 'dynamic_sidebar' ) , 'dynamic_sidebar' ); 
	foreach( array_filter((array)$sidebars) as $sidebar)
	{
		if(interrio_set($sidebar , 'topcopy')) continue ;
		
		$name = interrio_set( $sidebar, 'sidebar_name' );
		
		if( ! $name ) continue;
		$slug = interrio_bunch_slug( $name ) ;
		
		register_sidebar( array(
			'name' => $name,
			'id' =>  sanitize_title( $slug ) ,
			'before_widget' => '<div id="%1$s" class="side-bar widget sidebar_widget %2$s">',
			'after_widget' => "</div>",
			'before_title' => '<div class="sec-title"><h3 class="skew-lines">',
			'after_title' => '</h3></div>',
		) );		
	}
	
	update_option('wp_registered_sidebars' , $wp_registered_sidebars) ;
}
add_action( 'widgets_init', 'interrio_bunch_widget_init' );
// Update items in cart via AJAX
function interrio_load_head_scripts() {
	$options = _WSH()->option();
    if ( !is_admin() ) {
		$protocol = is_ssl() ? 'https://' : 'http://';
		$map_path = '?key='.interrio_set($options, 'map_api_key');
		wp_enqueue_script( 'map_api', ''.$protocol.'maps.google.com/maps/api/js'.$map_path, array(), false, false );
		wp_enqueue_script( 'jquery-googlemap', get_template_directory_uri().'/js/gmaps.js', array(), false, false );
	}
}
add_action( 'wp_enqueue_scripts', 'interrio_load_head_scripts' );
//global variables
function interrio_bunch_global_variable() {
    global $wp_query;
}

function interrio_enqueue_scripts() {
	$theme_options = _WSH()->option();
	$maincolor = str_replace( '#', '' , interrio_set( $theme_options, 'main_color_scheme', '#d5ac63' ) );
    //styles
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css' );
	wp_enqueue_style( 'fontawesome', get_template_directory_uri() . '/css/font-awesome.min.css' );
	wp_enqueue_style( 'hover', get_template_directory_uri() . '/css/hover.css' );
	wp_enqueue_style( 'bxslider', get_template_directory_uri() . '/css/jquery.bxslider.css' );
	wp_enqueue_style( 'owl-carousel', get_template_directory_uri() . '/css/owl.carousel.css' );
	wp_enqueue_style( 'owl-min', get_template_directory_uri() . '/css/owl.theme.default.min.css' );
	wp_enqueue_style( 'animate', get_template_directory_uri() . '/css/animate.min.css' );
	wp_enqueue_style( 'bootstrap-touchspin', get_template_directory_uri() . '/css/jquery.bootstrap-touchspin.css' );
	wp_enqueue_style( 'fancybox', get_template_directory_uri() . '/css/jquery.fancybox.css' );
	wp_enqueue_style( 'prettyPhoto', get_template_directory_uri() . '/css/prettyPhoto.css' );
	wp_enqueue_style( 'nouislider', get_template_directory_uri() . '/assets/price-filter/nouislider.css' );
	wp_enqueue_style( 'nouislider-pips', get_template_directory_uri() . '/assets/price-filter/nouislider.pips.css' );
	wp_enqueue_style( 'flexslider', get_template_directory_uri() . '/css/flexslider.css' );
	wp_enqueue_style( 'timePicker', get_template_directory_uri() . '/assets/timepicker/timePicker.css' );
	wp_enqueue_style( 'flaticon', get_template_directory_uri() . '/css/flaticon.css' );
	wp_enqueue_style( 'jquery-ui', get_template_directory_uri() . '/assets/jquery-ui-1.11.4/jquery-ui.css' );
	wp_enqueue_style( 'polyglot-language', get_template_directory_uri() . '/assets/language-switcher/polyglot-language-switcher.css' );
	wp_enqueue_style( 'interrio_main-style', get_stylesheet_uri() );
	wp_enqueue_style( 'interrio_custom-style', get_template_directory_uri() . '/css/custom.css' );
	wp_enqueue_style( 'interrio_responsive', get_template_directory_uri() . '/css/responsive.css' );
	if(class_exists('woocommerce')) wp_enqueue_style( 'interrio_woocommerce', get_template_directory_uri() . '/css/woocommerce.css' );
	wp_enqueue_style( 'interrio-main-color', get_template_directory_uri() . '/css/color.php?main_color='.$maincolor );
	wp_enqueue_style( 'interrio-color-panel', get_template_directory_uri() . '/css/color-panel.css' );
	
	
    //scripts
	wp_enqueue_script( 'jquery-ui-core');
	wp_enqueue_script( 'bootstrap', get_template_directory_uri().'/js/bootstrap.min.js', array(), false, true );
	wp_enqueue_script( 'bxslider', get_template_directory_uri().'/js/jquery.bxslider.min.js', array(), false, true );
	wp_enqueue_script( 'countTo', get_template_directory_uri().'/js/jquery.countTo.js', array(), false, true );
	wp_enqueue_script( 'owl-carousel', get_template_directory_uri().'/js/owl.carousel.min.js', array(), false, true );
	wp_enqueue_script( 'validate', get_template_directory_uri().'/js/validate.js', array(), false, true );
	wp_enqueue_script( 'mixitup', get_template_directory_uri().'/js/jquery.mixitup.min.js', array(), false, true );
	wp_enqueue_script( 'easing', get_template_directory_uri().'/js/jquery.easing.min.js', array(), false, true );
	wp_enqueue_script( 'map-helper', get_template_directory_uri().'/js/map-helper.js', array(), false, true );
	wp_enqueue_script( 'map-helper-2', get_template_directory_uri().'/js/map-helper-2.js', array(), false, true );
	wp_enqueue_script( 'fitvids', get_template_directory_uri().'/js/jquery.fitvids.js', array(), false, true );
	wp_enqueue_script( 'flexslider', get_template_directory_uri().'/js/jquery.flexslider.js', array(), false, true );
	wp_enqueue_script( 'timePicker', get_template_directory_uri().'/assets/timepicker/timePicker.js', array(), false, true );
	wp_enqueue_script( 'polyglot-language', get_template_directory_uri().'/assets/language-switcher/jquery.polyglot.language.switcher.js', array(), false, true );
	wp_enqueue_script( 'fancybox', get_template_directory_uri().'/js/jquery.fancybox.pack.js', array(), false, true );
	wp_enqueue_script( 'appear', get_template_directory_uri().'/js/jquery.appear.js', array(), false, true );
	wp_enqueue_script( 'isotope', get_template_directory_uri().'/js/isotope.js', array(), false, true );
	wp_enqueue_script( 'prettyPhoto', get_template_directory_uri().'/js/jquery.prettyPhoto.js', array(), false, true );
	wp_enqueue_script( 'interrio_main_script', get_template_directory_uri().'/js/custom.js', array(), false, true );
	if( is_singular() ) wp_enqueue_script('comment-reply');
	
}
add_action( 'wp_enqueue_scripts', 'interrio_enqueue_scripts' );

/*-------------------------------------------------------------*/
function interrio_theme_slug_fonts_url() {
    $fonts_url = '';
 
    /* Translators: If there are characters in your language that are not
    * supported by Lora, translate this to 'off'. Do not translate
    * into your own language.
    */
	$lato = _x( 'on', 'Oswald font: on or off', 'interrio' );
    $roboto = _x( 'on', 'Roboto font: on or off', 'interrio' );
 
    if ( 'off' !== $lato || 'off' !== $roboto ) {
        $font_families = array();
 
        if ( 'off' !== $lato ) {
            $font_families[] = 'Lato:100,100i,300,300i,400,400i,700,700i,900,900i';
        }
		
		if ( 'off' !== $roboto ) {
            $font_families[] = 'Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i';
        }
 
        $opt = _WSH()->option();
		if ( interrio_set( $opt, 'body_custom_font' ) ) {
			if ( $custom_font = interrio_set( $opt, 'body_font_family' ) )
				$font_families[] = $custom_font . ':300,300i,400,400i,600,700';
		}
		if ( interrio_set( $opt, 'use_custom_font' ) ) {
			$font_families[] = interrio_set( $opt, 'h1_font_family' ) . ':300,300i,400,400i,600,700';
			$font_families[] = interrio_set( $opt, 'h2_font_family' ) . ':300,300i,400,400i,600,700';
			$font_families[] = interrio_set( $opt, 'h3_font_family' ) . ':300,300i,400,400i,600,700';
			$font_families[] = interrio_set( $opt, 'h4_font_family' ) . ':300,300i,400,400i,600,700';
			$font_families[] = interrio_set( $opt, 'h5_font_family' ) . ':300,300i,400,400i,600,700';
			$font_families[] = interrio_set( $opt, 'h6_font_family' ) . ':300,300i,400,400i,600,700';
		}
		$font_families = array_unique( $font_families);
        
		
		$query_args = array(
            'family' => urlencode( implode( '|', $font_families ) ),
            'subset' => urlencode( 'latin,latin-ext' ),
        );
 
        $fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
    }
 
    return esc_url_raw( $fonts_url );
}
function interrio_theme_slug_scripts_styles() {
    wp_enqueue_style( 'interrio-theme-slug-fonts', interrio_theme_slug_fonts_url(), array(), null );
}
add_action( 'wp_enqueue_scripts', 'interrio_theme_slug_scripts_styles' );
/*---------------------------------------------------------------------*/
function interrio_add_editor_styles() {
    add_editor_style( 'custom-editor-style.css' );
}
add_action( 'admin_init', 'interrio_add_editor_styles' );
/**
 * WooCommerce Extra Feature
 * --------------------------
 *
 * Change number of related products on product page
 * Set your own value for 'posts_per_page'
 *
 */ 
function interrio_woo_related_products_limit() {
  global $product;
	
	$args['posts_per_page'] = 6;
	return $args;
}
add_filter( 'woocommerce_output_related_products_args', 'interrio_jk_related_products_args' );
  function interrio_jk_related_products_args( $args ) {
	$args['posts_per_page'] = 3; // 4 related products
	$args['columns'] = 3; // arranged in 2 columns
	return $args;
}

function SearchFilter($query) {
    // If 's' request variable is set but empty
    if (isset($_GET['s']) && empty($_GET['s']) && $query->is_main_query()){
        $query->is_search = true;
        $query->is_home = false;
    }
    return $query;}
add_filter('pre_get_posts','SearchFilter');