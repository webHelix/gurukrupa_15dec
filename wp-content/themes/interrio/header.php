<?php $options = _WSH()->option();
	interrio_bunch_global_variable();
	$icon_href = (interrio_set( $options, 'site_favicon' )) ? interrio_set( $options, 'site_favicon' ) : get_template_directory_uri().'/images/favicon.ico';
 ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ):?>
	<link rel="shortcut icon" href="<?php echo esc_url($icon_href);?>" type="image/x-icon">
	<link rel="icon" href="<?php echo esc_url($icon_href);?>" type="image/x-icon">
<?php endif;?>
<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div class="page-wrapper">
 	
    <?php if(interrio_set($options, 'preloader')):?>
		<!-- Preloader -->
		<div class="preloader"></div>
	<?php endif;?>
    
    <section class="top-bar-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-7">
                    <!--Start contact info left-->
                    <div class="contact-info-left clearfix">
                        <ul>
                        	<?php if(interrio_set($options, 'address')){ ?>
                            <li><span class="flaticon-building"></span><?php echo wp_kses_post(interrio_set($options, 'address')); ?></li>
                            <?php } if(interrio_set($options, 'email')){ ?>
                            <li><span class="flaticon-new-email-outline envelop"></span><?php echo wp_kses_post(interrio_set($options, 'email')); ?></li>
                        	<?php } ?>
                        </ul>
                    </div>
                    <!--End contact info left-->    
                </div>
                <div class="col-lg-5 col-md-5">
                    <!--Start contact info right-->
                    <div class="contact-info-right pull-right">
                    	<?php if(interrio_set($options, 'phone')){ ?>
                        <div class="phnumber">
                            <p><span class="flaticon-technology"></span><?php echo wp_kses_post(interrio_set($options, 'phone')); ?></p>     
                        </div>
                        <?php } ?>
                        
                        <?php if(interrio_set($options, 'lang_switcher')): ?>
                        <div class="language-switcher">
                            <div id="polyglotLanguageSwitcher">
                                <?php do_action('wpml_add_language_selector'); ?>
                            </div>
                        </div>
                        <?php endif;?>
                        
                        <?php $social_icons = interrio_set( $options, 'social_media' );
						if(!empty($social_icons)) { ?>
                        <div class="top-social-links">
                            <ul>
                            	<?php foreach( interrio_set( $social_icons, 'social_media' ) as $social_icon ):
								if( isset( $social_icon['tocopy' ] ) ) continue; ?>
                                <li><a href="<?php echo esc_url(interrio_set( $social_icon, 'social_link')); ?>" target="_blank"><i class="fa <?php echo esc_attr(interrio_set( $social_icon, 'social_icon')); ?>" aria-hidden="true"></i></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                        <?php } ?>
                        <div class="clearfix"></div>
                    </div>
                    <!--End contact info right--> 
                </div>
            </div>
        </div>
    </section>
    
    <!--Start header-search  area-->
    <section class="header-search">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="search-form pull-right">
                    <?php get_template_part('searchform3')?>    
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--End header-search  area-->  
    
    <!--Start header area-->
    <header class="header-area stricky">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="outer-box clearfix">
                        <!--Start logo-->
                        <div class="logo">
                        	<?php if(interrio_set($options, 'logo_image')):?>
                                <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url(interrio_set($options, 'logo_image'));?>" alt="<?php esc_html_e('Arctica', 'interrio');?>" title="<?php esc_html_e('Interrio', 'interrio');?>"></a>
                            <?php else:?>
                                <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url(get_template_directory_uri().'/images/resources/logo.png');?>" alt="<?php esc_html_e('Interrio', 'interrio');?>"></a>
                            <?php endif;?>
                        </div>
                        <!--End logo-->
                        <!--Start search box btn-->
                        <div class="search-box-btn">
                            <div class="toggle-search">
                                <button ><span class="icon fa fa-search"></span></button>    
                            </div>
                        </div>
                        <!--End search box btn-->
                        <!--Start cart btn-->
                        <?php /*if( function_exists( 'WC' ) ): global $woocommerce; ?>
                        <div class="cart-btn">
                            <a href="<?php echo esc_url($woocommerce->cart->get_cart_url()); ?>">
                                <span class="fa fa-shopping-bag carticon" aria-hidden="true"></span>
                                <span class="item-count"><?php echo balanceTags($woocommerce->cart->get_cart_contents_count()); ?></span>
                            </a>
                        </div>
                        <?php endif; */?>
                        <!--End cart btn-->
                        <!--Start mainmenu-->
                        <nav class="main-menu">
                            <div class="navbar-header">   	
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="navbar-collapse collapse clearfix">
                            	<ul class="navigation clearfix">
                                	<?php wp_nav_menu( array( 'theme_location' => 'main_menu', 'container_id' => 'navbar-collapse-1',
										'container_class'=>'navbar-collapse collapse navbar-right',
										'menu_class'=>'nav navbar-nav',
										'fallback_cb'=>false, 
										'items_wrap' => '%3$s', 
										'container'=>false,
										'walker'=> new Bunch_Bootstrap_walker()  
									) ); ?>
                                </ul>
                            </div>
                        </nav>
                        <!--End mainmenu-->
                    </div>
                </div>
            </div>
        </div>
    </header>  
    <!--End header area-->
 	
