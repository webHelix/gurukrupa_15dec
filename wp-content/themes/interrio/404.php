<?php $options = _WSH()->option();
get_header(); ?>

<!--Start 404 area-->
<section class="not-found-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="not-found-content text-center">
                    <h1><?php esc_html_e('404', 'interrio'); ?></h1>
                    <h3><?php esc_html_e("OOPPS! THE PAGE YOU WERE LOOKING FOR, COULDN'T BE FOUND.", 'interrio'); ?></h3>
                    <p><?php esc_html_e('Try the search below to find matching pages:', 'interrio'); ?></p>
                    <?php get_template_part('searchform2')?>
                </div>
            </div>
        </div>
    </div>
</section> 
<!--End 404 area-->

<?php get_footer(); ?>