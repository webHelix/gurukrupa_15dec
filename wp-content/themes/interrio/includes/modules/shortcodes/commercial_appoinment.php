<!--Start Appoinment area--> 
<section class="appoinment-area" style="background-image:url(<?php echo esc_url($bg_img); ?>);">
    <div class="container">
        <div class="sec-title pull-left">
            <h2><?php echo balanceTags($editor); ?></h2>
            <span class="decor"></span>
        </div>
        <div class="row">
            <div class="col-md-9">
                <?php echo do_shortcode($contact_text); ?>
            </div>
            <div class="col-md-3">
                <div class="business-hours">
                    <h3><i class="fa fa-clock-o" aria-hidden="true"></i> <?php esc_html_e('Business Hours:', 'interrio'); ?></h3>
                    <ul>
                        <li class="mon-fri"><span><?php esc_html_e('Monday - Friday', 'interrio'); ?></span> <?php echo balanceTags($monday_friday); ?></li>
                        <li class="Satu"><span><?php esc_html_e('Saturday', 'interrio'); ?></span> <?php echo balanceTags($saturday); ?></li>
                        <li class="sund"><span><?php esc_html_e('Sunday', 'interrio'); ?></span> <?php echo balanceTags($sunday); ?></li>
                    </ul>
                    <p><?php echo balanceTags($text); ?></p>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Appoinment area-->