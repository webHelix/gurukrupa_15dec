<!--Start working area-->
<section class="working-area">
    <div class="container">
        <div class="sec-title text-center">
            <h2><?php echo balanceTags($title); ?></h2>
            <p><?php echo balanceTags($text); ?></p>
        </div>  
        <div class="row">
        
        	<?php $count = 1; foreach( $atts['working_process'] as $key => $item ): ?>
            <!--Start single working item-->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-working-item">
                   <div class="icon-box">
                        <div class="icon">
                            <span class="<?php echo balanceTags($item->icon); ?>"></span>
                        </div>
                        <div class="count">
                            <h3><?php echo balanceTags($count++); ?></h3>
                        </div>    
                   </div>
                   <div class="text-box text-center">
                       <h3><?php echo balanceTags($item->title); ?></h3>
                       <p><?php echo balanceTags($item->text); ?></p>
                   </div>     
                </div>
            </div>
            <!--End single working item-->
            <?php endforeach; ?>
            
        </div>
    </div>
</section>
<!--End working area-->
