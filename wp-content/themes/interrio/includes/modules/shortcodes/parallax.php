<!--Parallax Image Section-->
<section class="parallax-image-section" style="background-image:url(<?php echo esc_url($bg_img);?>);">
	<div class="auto-container">
		<h2><?php echo balanceTags($title);?></h2>
		<h4><?php echo balanceTags($text);?></h4>
	</div>
</section>
<!--End Parallax Image Section-->