<!--Start design desc area-->
<section class="design-desc-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="tab-manu">
                    <ul class="tab-manu-items text-center" role="tablist">
                    	<?php $i=0; foreach( $atts['services_single_1'] as $key => $item ): ?>
                        <li class="single-item <?php if($i == 0) { echo 'active'; } ?>" data-tab-name="<?php echo 'tab-'.$i; ?>"><a href="#<?php echo 'tab-'.$i; ?>" aria-controls="<?php echo 'tab-'.$i; ?>" role="tab" data-toggle="tab" class="clearfix"><?php echo balanceTags($item->tab_name); ?></a></li>
                        <?php $i++; endforeach; ?>
                    </ul>    
                </div>
                <div class="tab-content">
                	<?php $i=0; foreach( $atts['services_single_1'] as $key => $item ): ?>
                    <!--Start single tab content-->
                    <div class="tab-pane fade in <?php if($i == 0) { echo 'active'; } ?>" id="<?php echo 'tab-'.$i; ?>">
                        <div class="inner-content">
                            <div class="row">
                                <div class="col-md-6">
                                    <!--Start design single carousel-->
                                    <div class="design-single-carousel carousel slide" data-ride="carousel">
                                        <ol class="carousel-indicators">
                                        	<?php $k=0; $images_id = explode(",", $item->multi_img);
											foreach($images_id as $image_id): ?>
                                            <li data-target=".design-single-carousel" data-slide-to="<?php echo esc_attr($k); ?>" class="<?php if($k == 0) { echo 'active'; } ?>"></li>
                                            <?php $k++; endforeach; ?>
                                        </ol>
                                        <div class="carousel-inner" role="listbox">
                                        	<?php $k=0; $images_id = explode(",", $item->multi_img);
											foreach($images_id as $image_id): ?>
                                            <div class="item <?php if($k == 0) { echo 'active'; } ?>">
                                                <div class="single-item">
                                                    <div class="img-holder">
                                                        <img src="<?php echo esc_url(wp_get_attachment_url($image_id)); ?>" alt="<?php echo balanceTags($item->title); ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <?php $k++; endforeach;?>
                                        </div>
                                    </div>
                                    <!--End design single carousel-->
                                </div>
                                <div class="col-md-6">
                                    <!--Start text-holder-->
                                    <div class="text-holder">
                                        <div class="sec-title-two">
                                            <h3><?php echo balanceTags($item->title); ?></h3>
                                        </div>
                                        <?php echo balanceTags($item->text); ?>
                                    </div>
                                    <!--End text-holder-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End single tab content-->
                    <?php $i++; endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End design desc area-->