<!--Start choose area--> 
<section class="choose-area" style="background-image:url(<?php echo balanceTags($bg_img); ?>);">
    <div class="container">
        <div class="sec-title text-center">
            <h2><?php echo balanceTags($title); ?></h2>
            <span class="border"></span>
        </div>
        <div class="row">
        	
            <?php foreach( $atts['choose_interior'] as $key => $item ): ?>
            <!--Start single item-->
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="single-item">
                    <div class="icon-holder">
                        <span class="<?php echo balanceTags($item->icon); ?>"></span>
                    </div>
                    <div class="text-holder">
                        <h3><?php echo balanceTags($item->title); ?></h3>
                        <p><?php echo balanceTags($item->text); ?></p>
                    </div>
                </div>
            </div>
            <!--End single item-->
            <?php endforeach; ?>
                                     
        </div>
    </div>    
</section>
<!--End choose area-->
