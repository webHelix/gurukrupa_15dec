<!--Start pricing  area-->
<section class="pricing-table-area">
    <div class="container">
        <div class="sec-title">
            <h2><?php echo balanceTags($editor); ?></h2>
            <span class="decor"></span>
        </div>
        <div class="row">
        	
            <?php foreach( $atts['pricing_plan'] as $key => $item ): ?>
            <!--Start single price box-->
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 text-center">
                <div class="single-price-box">
                    <div class="table-header">
                        <h3><?php echo balanceTags($item->price_title); ?></h3>
                    </div>
                    <div class="price">
                        <h1><span><?php echo balanceTags($item->sign); ?> </span><?php echo balanceTags($item->amount); ?><b><?php esc_html_e('/', 'interrio'); ?> <?php echo balanceTags($item->month); ?></b></h1>
                    </div>
                    <div class="price-list">
                        <ul>
                            <?php $fearures = explode("\n", ($item->text));
							foreach($fearures as $feature): ?>
							<li><?php echo balanceTags($feature); ?></li>
							<?php endforeach; ?>
                        </ul>
                    </div>
                    <div class="table-footer">
                        <a href="<?php echo esc_url($item->btn_link); ?>"><?php echo balanceTags($item->btn_title); ?></a>
                    </div>
                </div>
            </div>
            <!--Start single price box-->
            <?php endforeach; ?>
            
        </div>
    </div>    
</section>                  
<!--End pricing  area-->
