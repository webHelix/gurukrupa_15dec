<?php $query_args = array('post_type' => 'bunch_testimonials' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
if( $cat ) $query_args['testimonials_category'] = $cat;
$query = new WP_Query($query_args); ?>

<?php if($query->have_posts()): ?>

<!--Start Testimonial area-->
<section class="testimonial-v2-area">
    <div class="container">
        <div class="row">
        	
            <?php while($query->have_posts()): $query->the_post();
			global $post;
			$meta = _WSH()->get_meta(); ?>

            <!--Start single item-->
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="height:200px">
                <div class="single-item">

                <div class="col-lg-12 col-md-6 col-sm-6 col-xs-12">
                    <div class="text-holder">
                        <p><?php echo balanceTags(interrio_trim(get_the_content(), $text_limit)); ?></p>
                        
                         <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12" style=" width: 17%;">
                    
                        <?php //the_post_thumbnail('interrio_270x170'); ?>
                         
                                <img src="<?php echo esc_url(interrio_set($meta, 'client_img')); ?>" alt="<?php esc_html_e('Awesome Image', 'interrio'); ?>">
                        
                       <!-- <div class="quote-box">
                            <i class="fa fa-quote-right" aria-hidden="true"></i>
                        </div>!-->
              
                 </div>

                        <h3><?php the_title(); ?><?php esc_html_e(',', 'interrio'); ?> <span><?php echo wp_kses_post(interrio_set($meta, 'designation')); ?></span></h3>
                    </div>
                 </div>

                </div>
            </div>
            <!--End single item-->
            <?php endwhile; ?>
            
        </div>
    </div>
</section> 
<!--End Testimonial area-->

<?php endif; wp_reset_postdata(); ?>