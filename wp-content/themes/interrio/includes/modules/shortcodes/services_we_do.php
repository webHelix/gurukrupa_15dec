<?php $query_args = array('post_type' => 'bunch_services' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
if( $cat ) $query_args['services_category'] = $cat;
$query = new WP_Query($query_args); ?>

<?php if($query->have_posts()): ?>

<!--Start service area-->
<section class="service-area" style="background-image:url(<?php echo esc_html($bg_img); ?>);">
    <div class="container">
        <div class="sec-title">
            <h2><?php echo balanceTags($title); ?></h2>
            <span class="decor"></span>
        </div>
        <div class="row">
        	
            <?php while($query->have_posts()): $query->the_post();
			global $post;
			$services_meta = _WSH()->get_meta(); ?>
            <!--Start single service item-->
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="single-service-item">
                    <div class="img-holder">
                        <?php the_post_thumbnail('interrio_370x180'); ?>
                        <div class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="<?php echo esc_url(interrio_set($services_meta, 'ext_url')); ?>"><i class="fa fa-link" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-holder">
                        <h3><a href="<?php echo esc_url(interrio_set($services_meta, 'ext_url')); ?>"><?php the_title(); ?></a></h3>
                        <p><?php echo balanceTags(interrio_trim(get_the_content(), $text_limit)); ?></p>
                    </div>    
                </div>
            </div>
            <!--End single service item-->
            <?php endwhile; ?>
            
        </div>
    </div>
</section>
<!--End service area-->

<?php endif; wp_reset_postdata(); ?>