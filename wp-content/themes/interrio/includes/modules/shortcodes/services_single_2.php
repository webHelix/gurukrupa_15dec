<!--Start service v2 area-->
<section class="service-v2-area">
    <div class="container">
        <div class="row">
        	<?php foreach( $atts['services_single_2'] as $key => $item ): ?>
            <!--Start single item-->
            <div class="col-md-4">
                <div class="single-item">
                    <div class="img-holder">
                        <img src="<?php echo esc_url($item->img); ?>" alt="<?php esc_html_e('Awesome Image', 'interrio'); ?>">
                        <div class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="<?php echo esc_url($item->url); ?>"><?php esc_html_e('Free Quote', 'interrio'); ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-holder">
                        <div class="top">
                            <div class="icon-holder">
                                <span class="<?php echo balanceTags($item->icon); ?>"></span>
                            </div>
                            <div class="title-holder">
                                <h3><?php echo balanceTags($item->title); ?></h3>
                            </div>
                        </div>
                        <div class="text">
                            <p><?php echo balanceTags($item->text); ?></p>
                            <h3><?php echo balanceTags($item->pricing); ?></h3>
                            <span><?php echo balanceTags($item->features); ?></span>
                        </div>
                    </div>
                </div>
            </div>
            <!--End single item-->
            <?php endforeach; ?>
        </div>
    </div>    
</section>
<!--End service v2 area-->
