<!--Start contact v1 area-->
<section class="contact-v1-area">
    <div class="container">
        <div class="sec-title text-center">
            <h2><?php echo balanceTags($title); ?></h2>
            <p><?php echo balanceTags($text); ?></p>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="img-holder">
                    <img src="<?php echo esc_url($img); ?>" alt="">
                </div>
                <div class="contact-info">
                    <div class="row">
                        <!--Start single item-->
                        <div class="col-md-6">
                            <div class="single-item">
                                <div class="icon-holder">
                                    <span class="flaticon-building"></span>
                                </div>
                                <div class="text-holder">
                                    <h5>Address:</h5>
                                    <p><?php echo wp_kses_post($address); ?></p>
                                </div>
                            </div>
                        </div>
                        <!--End single item-->
                        <!--Start single item-->
                        <div class="col-md-6">
                            <div class="single-item">
                                <div class="icon-holder">
                                    <span class="flaticon-technology"></span>
                                </div>
                                <div class="text-holder">
                                    <h5>Phone:</h5>
                                    <p><?php echo wp_kses_post($phone); ?></p>
                                </div>
                            </div>
                        </div>
                        <!--End single item-->
                        <!--Start single item-->
                        <div class="col-md-6">
                            <div class="single-item">
                                <div class="icon-holder">
                                    <span class="flaticon-new-email-outline"></span>
                                </div>
                                <div class="text-holder">
                                    <h5><?php esc_html_e('Email Address:', 'interrio');?></h5>
                                    <p><?php echo sanitize_email($email); ?></p>
                                </div>
                            </div>
                        </div>
                        <!--End single item-->
                        <!--Start single item-->
                        <div class="col-md-6">
                            <div class="single-item">
                                <div class="icon-holder">
                                    <span class="flaticon-clock"></span>
                                </div>
                                <div class="text-holder">
                                    <h5>Working Hours:</h5>
                                    <p><?php echo balanceTags($working_hours); ?></p>
                                </div>
                            </div>
                        </div>
                        <!--End single item-->
                    </div>
                </div>    
            </div>
            <div class="col-md-6">
                <div class="contact-form-v1">
                    <?php echo do_shortcode($contact_text); ?>
                </div>
            </div>
             
        </div>
    </div>
</section>                                                                     
<!--End contact v1 area-->
