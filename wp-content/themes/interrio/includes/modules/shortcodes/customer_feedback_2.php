<?php $query_args = array('post_type' => 'bunch_testimonials' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
if( $cat ) $query_args['testimonials_category'] = $cat;
$query = new WP_Query($query_args); ?>

<?php if($query->have_posts()): ?>

<!--Start feedback area-->
<section class="feedback-area" style="background-image:url(<?php echo esc_url($bg_img); ?>);">
    <div class="container">
        <div class="sec-title text-center">
            <h2><?php echo balanceTags($title); ?></h2>
            <span class="border"></span>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="feedback-carousel">
                	
					<?php while($query->have_posts()): $query->the_post();
					global $post;
					$testimonials_meta = _WSH()->get_meta(); ?>
                    <!--Start single carousel item-->
                    <div class="single-feedback-item">
                        <div class="img-holder">
                            <?php the_post_thumbnail('interrio_95x95'); ?>
                        </div>
                        <div class="text-holder">
                            <h3><?php the_title(); ?><?php esc_html_e(',', 'interrio'); ?> <span><?php echo wp_kses_post(interrio_set($testimonials_meta, 'designation')); ?></span></h3>
                            <p><?php echo balanceTags(interrio_trim(get_the_content(), $text_limit)); ?></p>
                        </div>
                    </div>
                    <!--End single carousel item-->
                    <?php endwhile; ?>
                    
                </div>
            </div>
        </div>
    </div>
</section> 
<!--End feedback area-->

<?php endif; wp_reset_postdata();