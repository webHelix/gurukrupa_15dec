<!--Start slogan area-->
<section class="slogan-area" style="background-image:url(<?php echo esc_url($bg_img); ?>);">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="slogan">
                    <h2><?php echo balanceTags($text); ?></h2>
                </div>
            </div>
        </div>     
    </div>
</section>
<!--End slogan area-->