<!--Start about interrio area--> 
<section class="about-interrio-area">
    <div class="container">
        <div class="sec-title">
            <h2><?php echo balanceTags($editor); ?></h2>
            <span class="decor"></span>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="text-holder">
                    <h3><?php echo balanceTags($sub_title); ?></h3>
                    <p><?php echo balanceTags($text); ?></p>
                    <div class="signature-and-name">
                        <div class="signature">
                            <img src="<?php echo esc_url($signature); ?>" alt="<?php esc_html_e('Signature', 'interrio'); ?>">
                        </div>
                        <div class="name">
                            <h4><?php echo balanceTags($name); ?><?php esc_html_e(',', 'interrio'); ?></h4>
                            <p><?php echo wp_kses_post($designation); ?></p>
                        </div>
                    </div>   
                </div>
            </div>
            <div class="col-md-12" style="margin-top:10px">
            <?php foreach( $atts['mission_vision'] as $key => $item ): ?>
            <!--Start single item-->
            <div class="col-md-6">
                <div class="single-item">
                    <div class="img-holder" style="width: 50%;">
                        <img src="<?php echo esc_url($item->img); ?>" alt="<?php esc_html_e('Awesome Image', 'interrio'); ?>">
                        <div class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="<?php echo esc_url($item->url); ?>"><i class="fa fa-link" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-box">
                        <h3><?php echo balanceTags($item->title); ?></h3>
                        <p><?php echo balanceTags($item->text); ?></p>
                    </div>
                </div>
            </div>
            <!--End single item-->
            <?php endforeach; ?>
            </div>
            
        </div>
    </div>    
</section>           
<!--End about interrio area-->