<!--Restaurant Section-->
<section class="restaurant">
	<div class="auto-container">
		<div class="row clearfix">
			<!--Column-->
			<div class="column col-md-6 col-sm-6 col-xs-12">
				<div class="inner-box">
					<!--Sec Title-->
					<div class="sec-title">
						<h2><?php echo balanceTags($title);?> </h2>
						<h3><?php echo balanceTags($subtitle);?></h3>
					</div>
					<!--Content-->
					<div class="content">
						<div class="text">
							<p><?php echo balanceTags($text);?></p>
						</div>
						<a href="<?php echo esc_url($btn_link);?>" class="read-more theme-btn btn-style-one"><?php echo balanceTags($btn_title);?></a>
					</div>
					
				</div>
			</div>
			
			<!--Corousel Column-->
			<div class="corousel-column col-md-6 col-sm-6 col-xs-12">
				<div class="carousel-outer wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
					<!--Single Item Carousel-->
					<div class="single-item-carousel">
						
						<?php foreach($imgs as $img): ?>
							<div class="slide">
								<img src="<?php echo esc_url(wp_get_attachment_url($img)); ?>" alt="" />
							</div>
						<?php endforeach; ?>
						
					</div>
				</div>
			</div>
			
		</div>
	</div>
</section>
<!--End Restaurant Section-->
