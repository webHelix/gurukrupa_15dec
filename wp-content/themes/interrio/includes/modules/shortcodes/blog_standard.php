<?php global $post;
$query_args = array('post_type' => 'post' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
if( $cat ) $query_args['category_name'] = $cat;
$query = new WP_Query($query_args); ?>
   
<?php if($query->have_posts()): ?>

<!--Start blog area-->
<section id="blog-area" class="blog-standard-area">
    <div class="container">
        <div class="row">
        	
            <?php while($query->have_posts()): $query->the_post();
			global $post;
			$posts_meta = _WSH()->get_meta(); ?>
            <!--Start single blog post-->
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="single-blog-item">
                    <div class="img-holder">
                        <?php the_post_thumbnail('interrio_370x250'); ?>
                        <div class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="<?php echo esc_url(get_permalink(get_the_id())); ?>"><i class="fa fa-link"></i></a> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-holder">
                        <h3 class="blog-title"><a href="<?php echo esc_url(get_permalink(get_the_id())); ?>"><?php the_title(); ?></a></h3>
                        <ul class="meta-info">
                            <li><i class="fa fa-user" aria-hidden="true"></i><a href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) )); ?>"><?php the_author(); ?></a></li>
                            <li><i class="fa fa-clock-o" aria-hidden="true"></i><?php echo get_the_date('M d, Y'); ?></li>
                        </ul>
                        <div class="text">
                            <p><?php echo balanceTags(interrio_trim(get_the_content(), $text_limit)); ?></p>
                            <a class="readmore" href="<?php echo esc_url(get_permalink(get_the_id())); ?>"><?php esc_html_e('Read More', 'interrio'); ?><i class="fa fa-caret-right" aria-hidden="true"></i></a>
                        </div>
                    </div>    
                </div>
            </div>
            <!--End single blog post-->
            <?php endwhile; ?>
            
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="loadmore-button text-center">
                    <a class="thm-btn bg-cl-1" href="#"><?php esc_html_e('Load More', 'interrio'); ?></a>
                </div>
            </div>
        </div>
    </div>
</section> 
<!--End blog area-->

<?php endif; wp_reset_postdata(); ?>