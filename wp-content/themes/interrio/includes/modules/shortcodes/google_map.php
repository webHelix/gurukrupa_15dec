<!--Start Google map area-->
<section class="google-map-area">
    <div 
        class="google-map-1" 
        id="contact-google-map" 
        data-map-lat="<?php echo balanceTags($latitude); ?>" 
        data-map-lng="<?php echo balanceTags($longitude); ?>" 
        data-icon-path="<?php echo get_template_directory_uri().'/images/resources/map-marker.png'; ?>" 
        data-map-title="<?php echo wp_kses_post($address); ?>" 
        data-map-zoom="<?php echo balanceTags($zoom); ?>" 
        data-markers='{
        	<?php $i=1; $len = count($atts['office_address']);
			foreach( $atts['office_address'] as $key => $item ): ?>
            "marker-<?php echo esc_js($i); ?>": [<?php echo balanceTags($item->latitude); ?>, <?php echo balanceTags($item->longitude); ?>, "<h4><?php echo balanceTags($item->text); ?></h4><p><?php echo wp_kses_post($item->address); ?></p>"<?php if($i != 1){ ?>,"<?php echo get_template_directory_uri().'/images/resources/map-marker-1.png'; ?>"<?php } ?>]<?php if ($i == $len - 1){ ?>,<?php } ?>
            <?php $i++; endforeach; ?>
        }'>
    </div>
</section>
<!--End Google map area-->
