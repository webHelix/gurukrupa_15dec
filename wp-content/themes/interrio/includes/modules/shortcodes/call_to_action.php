<!--Start slogan v2 area-->
<section class="slogan-v2-area" style="background-image:url(<?php echo balanceTags($bg_img); ?>);">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="slogan-v2">
                    <div class="text">
                        <h2><?php echo balanceTags($text); ?></h2>
                    </div>
                    <div class="button pull-right">
                        <a href="<?php echo esc_url($btn_link); ?>"><?php echo balanceTags($btn_title); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</section>                   
<!--End slogan v2 area--> 
