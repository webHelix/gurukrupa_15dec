<!--Start welcome area-->
<section class="welcome-area">
    <div class="container clearfix">
        <div class="sec-title">
            <h2><?php echo balanceTags($editor); ?></h2>
            <span class="decor"></span>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="text-holder">
                    <h3><?php echo balanceTags($subtitle); ?></h3>
                    <?php echo balanceTags($text); ?>
                    <div class="bottom">
                       <div class="button">
                           <a class="thm-btn bg-cl-1" href="<?php echo esc_url($btn_link); ?>"><?php echo balanceTags($btn_title); ?></a>
                       </div>
                       <div class="title">
                           <h3><?php esc_html_e('Request Quote:', 'interrio'); ?> <span><?php echo wp_kses_post($email); ?></span></h3>
                       </div>     
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="gallery clearfix">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="video-gallery">
                                <img src="<?php echo esc_url($video_img); ?>" alt="<?php esc_html_e('Awesome Video Gallery', 'interrio'); ?>">
                                <div class="overlay-gallery">
                                    <div class="icon-holder">
                                        <div class="icon">
                                            <a href="<?php echo esc_url($video); ?>" class="video-fancybox" title="<?php esc_html_e('About Us', 'interrio'); ?>"><img src="<?php echo esc_url(get_template_directory_uri()).'/images/icon/play-btn.png'; ?>" alt="<?php esc_html_e('Play Button', 'interrio');?>"/></a>   
                                        </div>
                                    </div>    
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="gallery-bg-img">
                                <img src="<?php echo esc_url($img); ?>" alt="<?php esc_html_e('Awesome Image', 'interrio'); ?>">
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
            
        </div>
    </div>
</section>
<!--End welcome area-->