<?php $query_args = array('post_type' => 'bunch_testimonials' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
if( $cat ) $query_args['testimonials_category'] = $cat;
$query = new WP_Query($query_args); ?>

<?php if($query->have_posts()): ?>

<!--Start testimonial area-->
<section class="testimonial-area" style="background-image:url(<?php echo esc_url($bg_img); ?>);">
    <div class="container">
        <div class="sec-title text-center">
            <h2><?php echo balanceTags($title); ?></h2>
            <span class="border"></span>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="testimonials-carousel">
                
                	<?php while($query->have_posts()): $query->the_post();
					global $post;
					$testimonials_meta = _WSH()->get_meta(); ?>
                    <!--Start single slide item-->
                    <div class="single-slide-item">
                        <div class="img-box">
                            <?php the_post_thumbnail('interrio_394x264'); ?>
                            <div class="client-photo">
                                <img src="<?php echo esc_url(interrio_set($testimonials_meta, 'client_img')); ?>" alt="<?php esc_html_e('Awesome Image', 'interrio'); ?>">
                            </div>
                            <?php $rating = interrio_set($testimonials_meta, 'rating'); if(!empty($rating)){ ?>
                            <div class="review-box">
                                <span><?php esc_html_e('Project Rating:', 'interrio'); ?></span>
                                <ul>
                                	<?php for ($x = 1; $x <= 5; $x++) {
										if($x <= $rating) echo '<li><i class="fa fa-star"></i></li>'; else echo '<li><i class="fa fa-star-o"></i></li>';
									} ?>
                                </ul>
                            </div>
                            <?php } ?>
                        </div>
                        <div class="text-box">
                            <span class="flaticon-right"></span>
                            <div class="text">
                                <p><?php echo balanceTags(interrio_trim(get_the_content(), $text_limit)); ?></p>
                                <h3><?php the_title(); ?></h3>
                                <h4><?php echo wp_kses_post(interrio_set($testimonials_meta, 'designation')); ?></h4>
                            </div>
                        </div>
                    </div>
                    <!--End single slide item-->
                    <?php endwhile; ?>
                    
                </div>
            </div>
        </div>
    </div>
</section>
<!--End testimonial area-->

<?php endif; wp_reset_postdata();