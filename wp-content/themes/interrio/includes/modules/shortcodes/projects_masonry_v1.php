<?php $args = array('post_type' => 'bunch_projects', 'showposts'=>$num, 'orderby'=>$sort, 'order'=>$order);
$terms_array = explode(",",$exclude_cats);
if($exclude_cats) $args['tax_query'] = array(array('taxonomy' => 'projects_category','field' => 'id','terms' => $terms_array,'operator' => 'NOT IN',));
$query = new WP_Query($args);

$t = $GLOBALS['_bunch_base'];

$data_filtration = '';
$data_posts = ''; ?>

<?php if( $query->have_posts() ):
	ob_start();
	$count = 0; 
	$fliteration = array();
	
	while( $query->have_posts() ): $query->the_post();
		global  $post;
		$meta = get_post_meta( get_the_id(), '_bunch_portfolio_meta', true );
		$meta1 = _WSH()->get_meta();
		$post_terms = get_the_terms( get_the_id(), 'projects_category');
		foreach( (array)$post_terms as $pos_term ) $fliteration[$pos_term->term_id] = $pos_term;
		$temp_category = get_the_term_list(get_the_id(), 'projects_category', '', ', ');
		
		$post_terms = wp_get_post_terms( get_the_id(), 'projects_category'); 
		$term_slug = '';
		if( $post_terms ) foreach( $post_terms as $p_term ) $term_slug .= $p_term->slug.' ';

			$post_thumbnail_id = get_post_thumbnail_id($post->ID);
			$post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id );
			
			$term_list = wp_get_post_terms(get_the_id(), 'projects_category', array("fields" => "names"));
?>
			<!--Start single project item-->
            <div class="single-project-item filter-item <?php echo esc_attr($term_slug); ?> <?php if(interrio_set($meta1, 'extra_width') == 'extra_width') echo 'col-md-8 col-sm-6 col-xs-12'; else echo 'col-md-4 col-sm-6 col-xs-12'?>">
                <div class="img-holder">
                    <?php if(interrio_set($meta1, 'extra_width') == 'extra_width') 
						$image_size = 'interrio_770x600'; 
						elseif(interrio_set($meta1, 'extra_height') == 'extra_height')
						$image_size = 'interrio_370x600'; 
						else
						$image_size = 'interrio_370x260'; 
						the_post_thumbnail($image_size);
					?>
                    <div class="overlay">
                        <div class="box">
                            <div class="content">
                                <h3><a href="<?php echo esc_url(get_permalink(get_the_id()));?>"><?php the_title(); ?></a></h3>
                                <p><?php echo implode( ', ', (array)$term_list ); ?></p>
                                <div class="icon-holder">
                                    <a href="<?php echo esc_url($post_thumbnail_url); ?>" data-rel="prettyPhoto" title="<?php esc_html_e('Interrio Project', 'interrio'); ?>"><i class="fa fa-camera"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>   
            </div>
            <!--End single project item-->
           
	<?php endwhile; wp_reset_postdata();
$data_posts = ob_get_contents();
ob_end_clean();
endif;
ob_start();
$terms = get_terms(array('projects_category')); ?>

<!--Start project manasory v1 area--> 
<section id="project-area" class="pro-manasory-v1-area">
    <div class="container">
        <ul class="project-filter post-filter text-center">
            <li class="active" data-filter=".filter-item"><span><?php esc_attr_e('View All', 'interrio'); ?></span></li>
			<?php foreach( $fliteration as $t ): ?>
            <li data-filter=".<?php echo esc_attr(interrio_set($t, 'slug')); ?>"><span><?php echo balanceTags(interrio_set($t, 'name')); ?></span></li>
            <?php endforeach; ?>
        </ul>
        <div class="row project-content masonary-layout filter-layout">
            <?php echo balanceTags($data_posts); ?>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="loadmore-btn text-center">
                    <a class="thm-btn bg-cl-1" href="#"><?php esc_html_e('Load More', 'interrio'); ?></a>
                </div>
            </div>
        </div>
    </div>
</section>                            
<!--End project manasory v1 area-->
