<?php  $count = 1;
$query_args = array('post_type' => 'sh_menu' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
if( $cat ) $query_args['menu_category'] = $cat;
$query = new WP_Query($query_args) ; 
?>

<?php if($query->have_posts()):  ?>  

<!--Delicous Menu Section-->
<section class="delicous-section" style="background-image:url(<?php echo esc_url($bg_img);?>);">
	<div class="auto-container">
		<!--Delicous Inner-->
		<div class="delicous-inner">
			<div class="row clearfix">
			
				<!--Food Carousel Column-->
				<div class="food-carousel-column col-md-5 col-sm-12 col-xs-12">
					<div class="inner-box">
						<h2><?php echo balanceTags($title);?></h2>
						<div class="text"><?php echo balanceTags($text);?></div>
					
						<!--Single Item Carousel-->
						<div class="single-item-carousel">
							
							<?php foreach($imgs as $img): ?>
								<div class="slide"><img src="<?php echo esc_url(wp_get_attachment_url($img)); ?>" alt="" /></div>
							<?php endforeach; ?>
							
						</div>
						
					</div>
				</div>
				
				<!--Food Column-->
				<div class="food-column col-md-7 col-sm-12 col-xs-12">
					<div class="inner">
						<h3><?php echo balanceTags($subtitle);?></h3>
						
						<?php while($query->have_posts()): $query->the_post();
							global $post; 
							$menu_meta = _WSH()->get_meta();?>
							<!--Food Item-->
							<div class="food-item">
								<div class="inner-box">
									<!--Image Box-->
									<div class="image-box">
										<div class="new"><?php echo balanceTags(interrio_set($menu_meta, 'banner')); ?></div>
										<a href="<?php echo esc_url(interrio_set($menu_meta, 'ext_url')); ?>"><?php the_post_thumbnail('interrio_86x86', array('class' => 'img-responsive'));?></a>
									</div>
									<ul>
										<li class="clearfix"><span class="title-box"><a href="<?php echo esc_url(interrio_set($menu_meta, 'ext_url')); ?>"><?php echo wp_trim_words(get_the_title(), 25); ?></a></span> <span class="dots">...................................</span> <span class="price"><?php echo balanceTags(interrio_set($menu_meta, 'price')); ?></span></li>
										<?php /* for categories without anchor*/
											$term_list = wp_get_post_terms(get_the_id(), 'menu_category', array("fields" => "names")); ?>
										<li class="text"><?php echo implode( ', ', (array)$term_list );?></li>
									</ul>
								</div>
							</div>
						<?php endwhile; ?>
						
					</div>
				</div>
				
			</div>
		</div>
	</div>
</section>
<!--End Delicous Menu Section-->

<?php endif; ?>

<?php wp_reset_postdata();