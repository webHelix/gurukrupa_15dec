<!--Start press release content area-->
<section class="press-release-content-area">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="img-holder">
                    <img src="<?php echo esc_url($img); ?>" alt="<?php esc_html_e('Awesome Image', 'interrio'); ?>">
                </div>
            </div>
            <div class="col-md-8">
                <div class="press-release-caption">
                    <h3><?php echo balanceTags($title); ?></h3>
                </div>
                <div class="text">
                    <p><?php echo balanceTags($text); ?></p>
                </div>
                <div class="client-info">
                    <div class="signature">
                        <img src="<?php echo esc_url($signature); ?>" alt="<?php esc_html_e('Signature', 'interrio'); ?>">
                    </div>
                    <div class="client-name">
                        <h3><?php echo balanceTags($name); ?><?php esc_html_e(',', 'interrio'); ?></h3>
                        <p><?php echo wp_kses_post($designation); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> 
<!--End press release content area-->
 
<!--Start press release item area-->
<section class="press-release-item-area">
    <div class="container">
        <div class="row">
        	
            <?php foreach( $atts['press_release'] as $key => $item ): ?>
            <!--Start single item-->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-item">
                    <div class="img-holder">
                        <img src="<?php echo esc_url($item->img); ?>" alt="<?php esc_html_e('Awesome Image', 'interrio'); ?>">
                        <div class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="<?php echo esc_url($item->url); ?>"><i class="fa fa-link" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-holder">
                        <p><?php echo balanceTags($item->title); ?></p>
                        <a href="<?php echo esc_url($item->url); ?>"><?php echo balanceTags($item->date); ?></a>
                    </div>
                </div>
            </div>
            <!--End single item-->
            <?php endforeach; ?>
            
        </div>
    </div>
</section> 
<!--End press release item area-->
