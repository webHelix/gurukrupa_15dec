<!--Start Brand area-->  
<section class="brand-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="brand">
                	<?php $count = 1; foreach( $atts['our_clients'] as $key => $item ): ?>
                    <!--Start single item-->
                    <div class="single-item">
                        <a href="<?php echo esc_url($item->btn_link); ?>"><img src="<?php echo esc_url($item->img); ?>" alt=""></a>
                    </div>
                    <!--End single item-->
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Brand area-->     
