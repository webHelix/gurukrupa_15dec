<!--Start fact counter area-->
<div class="fact-counter-area">
    <div class="container">
        <div class="sec-title">
            <h2><?php echo balanceTags($title); ?></h2>
            <span class="decor"></span>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="fact-counter-carousel">
                	
                    <?php foreach( $atts['interesting_facts'] as $key => $item ): ?>
                    <div class="single-item">
                        <p><?php echo balanceTags($item->text); ?></p>
                    </div>
                    <?php endforeach; ?>
                    
                </div>
            </div>
            <div class="col-md-8">
                <div class="row">
                	
                    <?php foreach( $atts['fun_facts'] as $key => $item ): ?>
                    <!--Start single fact counter-->
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="single-fact-counter text-center">
                            <div class="box">
                                <h2><span class="timer" data-from="<?php echo balanceTags($item->counter_start); ?>" data-to="<?php echo balanceTags($item->counter_end); ?>" data-speed="5000" data-refresh-interval="50"><?php echo balanceTags($item->counter_end); ?></span><i><?php echo balanceTags($item->counter_sign); ?></i></h2>
                                <div class="icon-holder">
                                    <span class="<?php echo balanceTags($item->icon); ?>"></span>
                                </div>
                            </div>
                            <div class="title">
                                <h3><?php echo balanceTags($item->title); ?></h3>
                            </div>
                        </div>
                    </div>
                    <!--End single fact counter-->
                    <?php endforeach; ?>
                    
                </div>                
            </div>
        </div>
    </div>
</div>
<!--End fact counter area-->
