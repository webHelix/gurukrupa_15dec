<?php $args = array('post_type' => 'bunch_projects', 'showposts'=>$num, 'orderby'=>$sort, 'order'=>$order);
$terms_array = explode(",",$exclude_cats);
if($exclude_cats) $args['tax_query'] = array(array('taxonomy' => 'projects_category','field' => 'id','terms' => $terms_array,'operator' => 'NOT IN',));
$query = new WP_Query($args);

$t = $GLOBALS['_bunch_base'];

$data_filtration = '';
$data_posts = ''; ?>

<?php if( $query->have_posts() ):
	ob_start();
	$count = 0; 
	$fliteration = array();
	
	while( $query->have_posts() ): $query->the_post();
		global  $post;
		$meta = get_post_meta( get_the_id(), '_bunch_portfolio_meta', true );//printr($meta);
		$meta1 = _WSH()->get_meta();
		$post_terms = get_the_terms( get_the_id(), 'projects_category');// printr($post_terms); exit();
		foreach( (array)$post_terms as $pos_term ) $fliteration[$pos_term->term_id] = $pos_term;
		$temp_category = get_the_term_list(get_the_id(), 'projects_category', '', ', ');
		
		$post_terms = wp_get_post_terms( get_the_id(), 'projects_category'); 
		$term_slug = '';
		if( $post_terms ) foreach( $post_terms as $p_term ) $term_slug .= $p_term->slug.' ';

			$post_thumbnail_id = get_post_thumbnail_id($post->ID);
			$post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id );
			
			$term_list = wp_get_post_terms(get_the_id(), 'projects_category', array("fields" => "names"));
?>
			<!--Start single project-->
            <div class="single-project filter-item <?php echo esc_attr($term_slug); ?> <?php if(interrio_set($meta1, 'extra_width') == 'extra_width') echo 'col-md-6 col-sm-6 col-xs-12'; else echo 'col-md-3 col-sm-6 col-xs-12'?>">
                <div class="single-project-item">
                    <div class="img-holder">
                        <?php if(interrio_set($meta1, 'extra_width') == 'extra_width') 
							$image_size = 'interrio_580x500'; 
							elseif(interrio_set($meta1, 'extra_height') == 'extra_height')
							$image_size = 'interrio_285x500'; 
							else
							$image_size = 'interrio_285x245'; 
							the_post_thumbnail($image_size);
						?>
                        <div class="overlay">
                            <div class="box">
                                <div class="content">
                                    <h3><a href="<?php echo esc_url(get_permalink(get_the_id()));?>"><?php the_title(); ?></a></h3>
                                    <p><?php echo implode( ', ', (array)$term_list ); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>   
                </div>
            </div>
            <!--End single project-->
           
	<?php endwhile; wp_reset_postdata();
$data_posts = ob_get_contents();
ob_end_clean();
endif;
ob_start();
$terms = get_terms(array('projects_category')); ?>

<!--Start Commercial design area-->
<section id="commercial-design-area" class="latest-project-area">
    <div class="container">
        <div class="container">
            <div class="sec-title pull-left">
                <h2><?php echo balanceTags($editor); ?></h2>
                <span class="decor"></span>
            </div>   
            <ul class="project-filter post-filter pull-right">
            	<li class="active" data-filter=".filter-item"><span><?php esc_attr_e('View All', 'interrio'); ?></span></li>
				<?php foreach( $fliteration as $t ): ?>
                <li data-filter=".<?php echo esc_attr(interrio_set($t, 'slug')); ?>"><span><?php echo balanceTags(interrio_set($t, 'name')); ?></span></li>
                <?php endforeach; ?>
            </ul>
        </div>
        <div class="row project-content masonary-layout filter-layout">
        	<?php echo balanceTags($data_posts); ?>
        </div>     
    </div>
</section>
<!--End Commercial design area-->
