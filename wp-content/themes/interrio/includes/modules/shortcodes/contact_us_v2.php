<!--Start contact v1 area-->
<section class="contact-v2-area">
    <div class="container">
        <div class="row">
        	<?php foreach( $atts['office_address'] as $key => $item ): ?>
            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                <div class="contact-info">
                    <div class="title">
                        <h3><?php echo balanceTags($item->title); ?></h3>
                    </div>
                    <ul class="contact-address">
                        <li>
                            <div class="icon-holder">
                                <span class="flaticon-building"></span>
                            </div>
                            <div class="text-holder">
                                <p><?php echo wp_kses_post($item->address); ?></p>
                            </div>
                        </li>
                        <li>
                            <div class="icon-holder">
                                <span class="flaticon-technology"></span>
                            </div>
                            <div class="text-holder">
                                <p><?php echo wp_kses_post($item->phone); ?></p>
                            </div>
                        </li>
                        <li>
                            <div class="icon-holder">
                                <span class="flaticon-new-email-outline"></span>
                            </div>
                            <div class="text-holder">
                                <p><?php echo wp_kses_post($item->email); ?></p>
                            </div>
                        </li>
                        <li>
                            <div class="icon-holder">
                                <span class="flaticon-clock"></span>
                            </div>
                            <div class="text-holder">
                                <p><?php echo balanceTags($item->working_hours); ?></p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <?php endforeach; ?>
            <div class="col-lg-6 col-md-4 col-sm-12 col-xs-12">
                <div class="google-map-area">
                    <div 
                        class="google-map-2"
                        id="contact-v2-google-map"
                        data-map-lat="<?php echo balanceTags($latitude); ?>" 
                        data-map-lng="<?php echo balanceTags($longitude); ?>" 
                        data-icon-path="<?php echo get_template_directory_uri().'/images/resources/map-marker-2.png'; ?>" 
                        data-map-title="<?php echo wp_kses_post($address); ?>" 
                        data-map-zoom="<?php echo balanceTags($zoom); ?>"
                        data-markers='{
                            "marker-1": [<?php echo balanceTags($latitude); ?>, <?php echo balanceTags($longitude); ?>, "<h4><?php echo balanceTags($office_name); ?></h4><p><?php echo wp_kses_post($address); ?></p>"]
                        }'>
                    </div>
                </div>
            </div> 
        </div>
    </div>
</section>                                                                     
<!--End contact v1 area-->
 
<!--Start contact v2 form area-->
<section class="contact-v2-form-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="contact-v2-form">
                    <div class="sec-title text-center">
                        <h2><?php echo balanceTags($contact_title); ?></h2>
                        <p><?php echo balanceTags($text); ?></p>
                    </div>
                    <?php echo do_shortcode($contact_text); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End contact v2 form area-->
