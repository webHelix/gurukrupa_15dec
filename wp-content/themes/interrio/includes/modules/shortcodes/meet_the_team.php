<?php $query_args = array('post_type' => 'bunch_team' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
if( $cat ) $query_args['team_category'] = $cat;
$query = new WP_Query($query_args); ?>

<?php if($query->have_posts()): ?>

<!--Start our team area--> 
<section class="team-area">
    <div class="container">
        <div class="sec-title">
            <h2><?php echo balanceTags($editor); ?></h2>
            <span class="decor"></span>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="team-members">
                	
                    <?php while($query->have_posts()): $query->the_post();
					global $post;
					$meta = _WSH()->get_meta(); ?>
                    <!--Start single team member-->
                    <div class="single-team-member">
                        <div class="img-holder">
                            <?php the_post_thumbnail('interrio_350x250'); ?>
                            <div class="overlay">
                                <div class="box">
                                    <div class="content">
                                    	<?php if($socials = interrio_set($meta, 'bunch_team_social')):?>
                                        <ul>
                                            <?php foreach($socials as $key => $value): ?>
                                            <li><a href="<?php echo esc_url(interrio_set($value, 'social_link')); ?>" target="_blank"><i class="fa <?php echo esc_attr(interrio_set($value, 'social_icon')); ?>" aria-hidden="true"></i></a></li>
                                            <?php endforeach;?>
                                        </ul>
                                        <?php endif;?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="text-holder text-center">
                            <h3><?php the_title(); ?></h3>
                            <p><?php echo wp_kses_post(interrio_set($meta, 'designation'));?></p>
                        </div>
                    </div>    
                    <!--End single team member-->
                    <?php endwhile; ?>
                    
                </div>
            </div>
        </div>
        <!--Start bottom text-->
        <div class="row">
            <div class="col-md-12">
                <div class="bottom-text text-center">
                    <h3><?php echo balanceTags($text); ?></h3>
                </div>
            </div>
        </div>
        <!--End bottom text-->
    </div>
</section>
<!--End our team area-->

<?php endif; wp_reset_postdata();