<?php global $post;
$query_args = array('post_type' => 'post' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
if( $cat ) $query_args['category_name'] = $cat;
$query = new WP_Query($query_args); ?>
   
<?php if($query->have_posts()): ?>

<!--Start latest blog area-->
<section class="latest-blog-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="sec-title pull-left">
                    <h2><?php echo balanceTags($editor); ?></h2>
                    <span class="decor"></span>
                </div>
                <div class="more-blog-button pull-right">
                    <a class="thm-btn bg-cl-1" href="<?php echo esc_url($btn_link); ?>"><?php echo balanceTags($btn_title); ?></a> 
                </div>
            </div>
        </div>   
        <div class="row">
        	
            <?php while($query->have_posts()): $query->the_post();
			global $post;
			$posts_meta = _WSH()->get_meta(); ?>
            <!--Start single blog item-->
            <div class="col-md-4">
                <div class="single-blog-item">
                    <div class="img-holder">
                        <?php the_post_thumbnail('interrio_370x250'); ?>
                        <div class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="<?php echo esc_url(get_permalink(get_the_id())); ?>"><i class="fa fa-link" aria-hidden="true"></i></a> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-holder">
                        <h3 class="blog-title"><a href="<?php echo esc_url(get_permalink(get_the_id())); ?>"><?php the_title(); ?></a></h3>
                        <ul class="meta-info">
                            <li><i class="fa fa-user" aria-hidden="true"></i><a href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) )); ?>"><?php the_author(); ?></a></li>
                            <li><i class="fa fa-clock-o" aria-hidden="true"></i><?php echo get_the_date('F d, Y'); ?></li>
                        </ul>
                        <div class="text">
                            <p><?php echo balanceTags(interrio_trim(get_the_content(), $text_limit)); ?></p>
                            <a class="readmore" href="<?php echo esc_url(get_permalink(get_the_id())); ?>"><?php esc_html_e('Read More', 'interrio'); ?><i class="fa fa-caret-right" aria-hidden="true"></i></a>
                        </div>
                    </div>    
                </div>    
            </div>
            <!--End single blog item-->
            <?php endwhile; ?>
            
        </div>
    </div>
</section>
<!--End latest blog area-->

<?php endif; wp_reset_postdata(); ?>