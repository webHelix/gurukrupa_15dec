<?php
///----Blog widgets---

/// Recent Posts 
class Bunch_Recent_Post extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'Bunch_Recent_Post', /* Name */esc_html__('Interrio Recent Posts','interrio'), array( 'description' => esc_html__('Show the recent posts in sidebar', 'interrio' )) );
	}
 

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		
		echo balanceTags($before_widget); ?>
		
        <!-- Recent Posts -->
		<?php echo balanceTags($before_title.$title.$after_title); ?>
    
        <?php $query_string = 'posts_per_page='.$instance['number'];
        if( $instance['cat'] ) $query_string .= '&cat='.$instance['cat'];
        
        $this->posts($query_string);
        ?>
                    
		<?php echo balanceTags($after_widget);
	}
 
 
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['number'] = $new_instance['number'];
		$instance['cat'] = $new_instance['cat'];
		
		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ( $instance ) ? esc_attr($instance['title']) : esc_html__('Recent Posts', 'interrio');
		$number = ( $instance ) ? esc_attr($instance['number']) : 3;
		$cat = ( $instance ) ? esc_attr($instance['cat']) : '';?>
			
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title: ', 'interrio'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('number')); ?>"><?php esc_html_e('No. of Posts:', 'interrio'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('number')); ?>" name="<?php echo esc_attr($this->get_field_name('number')); ?>" type="text" value="<?php echo esc_attr( $number ); ?>" />
        </p>
       
    	<p>
            <label for="<?php echo esc_attr($this->get_field_id('cat')); ?>"><?php esc_html_e('Category', 'interrio'); ?></label>
            <?php wp_dropdown_categories( array('show_option_all'=>esc_html__('All Categories', 'interrio'), 'selected'=>$cat, 'class'=>'widefat', 'name'=>$this->get_field_name('cat')) ); ?>
        </p>
            
		<?php 
	}
	
	function posts($query_string)
	{
		$query = new WP_Query($query_string);
		if( $query->have_posts() ):?>
        	
            <ul class="recent-post">
                <?php while( $query->have_posts() ): $query->the_post(); ?>
                <li>
                    <div class="img-holder">
                        <?php the_post_thumbnail('interrio_70x80'); ?>
                        <div class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="<?php echo esc_url(get_permalink(get_the_id())); ?>"><i class="fa fa-link" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="title">
                        <h3><a href="<?php echo esc_url(get_permalink(get_the_id())); ?>"><?php echo wp_trim_words( get_the_title(), 6, '...' );?></a></h3>
                        <p><?php echo get_the_date('F d, Y');?></p>
                    </div>
                </li>
            	<?php endwhile; ?>
            </ul>
            
        <?php endif;
		wp_reset_postdata();
    }
}

/// About Us
class Bunch_About_us extends WP_Widget
{
	
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'Bunch_About_us', /* Name */esc_html__('Interrio About Us','interrio'), array( 'description' => esc_html__('Show the About Us sidebar', 'interrio' )) );
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$options = _WSH()->option();
		$title = apply_filters( 'widget_title', $instance['title'] );
		echo balanceTags($before_widget);?>
        	
            <?php echo balanceTags($before_title.$title.$after_title); ?>
      		
            <div class="text-box">
                <p><?php echo balanceTags($instance['content']); ?></p>
            </div>
            
		<?php
		
		echo balanceTags($after_widget);
	}
	
	
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		
		$instance['title'] = $new_instance['title'];
		$instance['content'] = $new_instance['content'];

		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ($instance) ? esc_attr($instance['title']) : 'About Us';
		$content = ($instance) ? esc_attr($instance['content']) : '';
		?>
        
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title:', 'interrio'); ?></label>
            <input placeholder="<?php esc_html_e('Title', 'interrio');?>" class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('content')); ?>"><?php esc_html_e('Content:', 'interrio'); ?></label>
            <textarea class="widefat" id="<?php echo esc_attr($this->get_field_id('content')); ?>" name="<?php echo esc_attr($this->get_field_name('content')); ?>" ><?php echo balanceTags($content); ?></textarea>
        </p>
                
		<?php 
	}
	
}

///----footer widgets---
//About Us
class Bunch_Contact_Details extends WP_Widget
{
	
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'Bunch_Contact_Details', /* Name */esc_html__('Interrio Contact Detail','interrio'), array( 'description' => esc_html__('Show the information about company', 'interrio' )) );
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$options = _WSH()->option();
		echo balanceTags($before_widget);?>
      		
            <div class="single-footer-widget pd-bottom">
                <!--Footer Column-->
                <div class="footer-logo">
                    <a href="<?php echo esc_url(home_url('/')); ?>">
                        <img src="<?php echo esc_url($instance['logo_url']); ?>" alt="<?php esc_html_e('Footer Logo', 'interrio'); ?>">
                    </a>
                </div>
                <div class="interrio-info">
                    <p><?php echo balanceTags($instance['content']); ?></p>
                </div>
                <ul class="footer-contact-info">
                    <li>
                        <div class="icon-holder">
                            <span class="flaticon-building"></span>
                        </div>
                        <div class="text-holder">
                            <p><?php echo wp_kses_post($instance['address']); ?></p>
                        </div>
                    </li>
                    <li>
                        <div class="icon-holder">
                            <span class="flaticon-technology"></span>
                        </div>
                        <div class="text-holder">
                            <p><?php echo wp_kses_post($instance['phone']); ?></p>
                        </div>
                    </li>
                    <li>
                        <div class="icon-holder">
                            <span class="flaticon-e-mail-envelope"></span>
                        </div>
                        <div class="text-holder">
                            <p><?php echo sanitize_email($instance['email']); ?></p>
                        </div>
                    </li>
                    <li>
                        <div class="icon-holder time">
                            <span class="flaticon-clock"></span>
                        </div>
                        <div class="text-holder">
                            <p><?php echo balanceTags($instance['working_hours']); ?></p>
                        </div>
                    </li>
                </ul>
            </div>
            
		<?php
		
		echo balanceTags($after_widget);
	}
	
	
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		
		$instance['logo_url'] = $new_instance['logo_url'];
		$instance['content'] = $new_instance['content'];
		$instance['address'] = $new_instance['address'];
		$instance['phone'] = $new_instance['phone'];
		$instance['email'] = $new_instance['email'];
		$instance['working_hours'] = $new_instance['working_hours'];

		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$logo_url = ( $instance ) ? esc_attr($instance['logo_url']) : 'http://wp1.themexlab.com/newwp/interrio/wp-content/themes/interrio/images/footer/footer-logo.png';
		$content = ($instance) ? esc_attr($instance['content']) : '';
		$address = ( $instance ) ? esc_attr($instance['address']) : '256 Interrio the good, New York City';
		$phone = ( $instance ) ? esc_attr($instance['phone']) : '(+321) 567 89 0123';
		$email = ( $instance ) ? esc_attr($instance['email']) : 'Supportyou@Interrio.com';
		$working_hours = ( $instance ) ? esc_attr($instance['working_hours']) : 'Mon - Sat: 09.00am to 18.00pm';
		?>
        
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('logo_url')); ?>"><?php esc_html_e('Logo URL:', 'interrio'); ?></label>
            <input placeholder="<?php esc_html_e('Logo URL', 'interrio');?>" class="widefat" id="<?php echo esc_attr($this->get_field_id('logo_url')); ?>" name="<?php echo esc_attr($this->get_field_name('logo_url')); ?>" type="text" value="<?php echo esc_attr($logo_url); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('content')); ?>"><?php esc_html_e('Content:', 'interrio'); ?></label>
            <textarea class="widefat" id="<?php echo esc_attr($this->get_field_id('content')); ?>" name="<?php echo esc_attr($this->get_field_name('content')); ?>" ><?php echo balanceTags($content); ?></textarea>
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('address')); ?>"><?php esc_html_e('Address:', 'interrio'); ?></label>
            <input placeholder="<?php esc_html_e('Address', 'interrio');?>" class="widefat" id="<?php echo esc_attr($this->get_field_id('address')); ?>" name="<?php echo esc_attr($this->get_field_name('address')); ?>" type="text" value="<?php echo esc_attr($address); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('phone')); ?>"><?php esc_html_e('Phone:', 'interrio'); ?></label>
            <input placeholder="<?php esc_html_e('Phone', 'interrio');?>" class="widefat" id="<?php echo esc_attr($this->get_field_id('phone')); ?>" name="<?php echo esc_attr($this->get_field_name('phone')); ?>" type="text" value="<?php echo esc_attr($phone); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('email')); ?>"><?php esc_html_e('Email:', 'interrio'); ?></label>
            <input placeholder="<?php esc_html_e('Email', 'interrio');?>" class="widefat" id="<?php echo esc_attr($this->get_field_id('email')); ?>" name="<?php echo esc_attr($this->get_field_name('email')); ?>" type="text" value="<?php echo esc_attr($email); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('working_hours')); ?>"><?php esc_html_e('Working Hours:', 'interrio'); ?></label>
            <input placeholder="<?php esc_html_e('Working Hours', 'interrio');?>" class="widefat" id="<?php echo esc_attr($this->get_field_id('working_hours')); ?>" name="<?php echo esc_attr($this->get_field_name('working_hours')); ?>" type="text" value="<?php echo esc_attr($working_hours); ?>" />
        </p>
                
		<?php 
	}
	
}

/// Popular Posts 
class Bunch_Popular_Post extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'Bunch_Popular_Post', /* Name */esc_html__('Interrio Popular Posts','interrio'), array( 'description' => esc_html__('Show the popular posts in footer', 'interrio' )) );
	}
 

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		
		echo balanceTags($before_widget); ?>
		
        <!-- Recent Posts -->
    	<div class="single-footer-widget pd-bottom">
			<?php echo balanceTags($before_title.$title.$after_title); ?>
            
			<?php $query_string = 'posts_per_page='.$instance['number'];
            if( $instance['cat'] ) $query_string .= '&cat='.$instance['cat'];
             
            
            $this->posts($query_string);
            ?>
        </div>
                    
		<?php echo balanceTags($after_widget);
	}
 
 
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['number'] = $new_instance['number'];
		$instance['cat'] = $new_instance['cat'];
		
		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ( $instance ) ? esc_attr($instance['title']) : esc_html__('Popular Posts', 'interrio');
		$number = ( $instance ) ? esc_attr($instance['number']) : 3;
		$cat = ( $instance ) ? esc_attr($instance['cat']) : '';?>
			
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title: ', 'interrio'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('number')); ?>"><?php esc_html_e('No. of Posts:', 'interrio'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('number')); ?>" name="<?php echo esc_attr($this->get_field_name('number')); ?>" type="text" value="<?php echo esc_attr( $number ); ?>" />
        </p>
       
    	<p>
            <label for="<?php echo esc_attr($this->get_field_id('cat')); ?>"><?php esc_html_e('Category', 'interrio'); ?></label>
            <?php wp_dropdown_categories( array('show_option_all'=>esc_html__('All Categories', 'interrio'), 'selected'=>$cat, 'class'=>'widefat', 'name'=>$this->get_field_name('cat')) ); ?>
        </p>
            
		<?php 
	}
	
	function posts($query_string)
	{
		$query = new WP_Query($query_string); 
		if( $query->have_posts() ):?>
        	
            <ul class="popular-news clearfix">
                <?php while( $query->have_posts() ): $query->the_post(); ?>
                <li class="single-popular-news-item clearfix">
                    <div class="img-holder">
                        <?php the_post_thumbnail('interrio_80x80'); ?>
                        <div class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="<?php echo esc_url(get_permalink(get_the_id())); ?>"><i class="fa fa-link" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-holder">
                        <a href="<?php echo esc_url(get_permalink(get_the_id())); ?>"><p><?php echo wp_trim_words( get_the_title(), 8, '...' );?></p></a>
                        <ul class="info">
                            <li><?php esc_html_e('by', 'interrio'); ?> <?php the_author(); ?></li>
                            <li><?php echo get_the_date('F d, Y');?></li>  
                        </ul>    
                    </div>
                </li>
            	<?php endwhile; ?>
            </ul>
            
        <?php endif;
		wp_reset_postdata();
    }
}

//Contact Us
class Bunch_Newsletter_Projects extends WP_Widget
{
	
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'Bunch_Newsletter_Projects', /* Name */esc_html__('Interrio Newsletter & Project','interrio'), array( 'description' => esc_html__('Show the Newsletter & Latest Projects', 'interrio' )) );
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title1 = apply_filters( 'widget_title', $instance['title1'] );
		$title2 = apply_filters( 'widget_title', $instance['title2'] );
		
		echo balanceTags($before_widget);?>
      		
            <div class="single-footer-widget pd-bottom">
                <?php echo balanceTags($before_title.$title1.$after_title); ?>
                <div class="subscribe-form">
                    <p>Subscribe to our newsletter!</p> 
                    <form action="http://feedburner.google.com/fb/a/mailverify">
                    	<input type="hidden" id="uri2" name="uri" value="<?php echo balanceTags($instance['id']); ?>">
                        <input type="text" name="email" placeholder="<?php esc_html_e('Enter Your Email...', 'interrio');?>">
                        <button type="submit"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                    </form>
                    <h4><?php echo balanceTags($instance['content']); ?></h4>
                </div>
                <!--Start latest project-->
                <div class="latest-project">
                    <?php echo balanceTags($before_title.$title2.$after_title); ?>
                    <?php $args = array('post_type' => 'bunch_projects', 'showposts'=>$instance['number']);
					if( $instance['cat'] ) $args['tax_query'] = array(array('taxonomy' => 'projects_category','field' => 'id','terms' => (array)$instance['cat']));
					
					$this->posts($args);  
					?>
                </div>
            </div>
            
		<?php
		
		echo balanceTags($after_widget);
	}
	
	
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['title1'] = strip_tags($new_instance['title1']);
		$instance['title2'] = strip_tags($new_instance['title2']);
		$instance['content'] = $new_instance['content'];
		$instance['id'] = $new_instance['id'];
		$instance['number'] = $new_instance['number'];
		$instance['cat'] = $new_instance['cat'];

		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$title1 = ($instance) ? esc_attr($instance['title1']) : 'Newsletter';
		$title2 = ($instance) ? esc_attr($instance['title2']) : 'Latest Projects';
		$content = ($instance) ? esc_attr($instance['content']) : '';
		$id = ( $instance ) ? esc_attr($instance['id']) : '';
		$number = ( $instance ) ? esc_attr($instance['number']) : 4;
		$cat = ( $instance ) ? esc_attr($instance['cat']) : '';
	?>
        
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title1')); ?>"><?php esc_html_e('Newsletter Title:', 'interrio'); ?></label>
            <input placeholder="<?php esc_html_e('Newsletter Title', 'interrio');?>" class="widefat" id="<?php echo esc_attr($this->get_field_id('title1')); ?>" name="<?php echo esc_attr($this->get_field_name('title1')); ?>" type="text" value="<?php echo esc_attr($title1); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('content')); ?>"><?php esc_html_e('Newsletter Content:', 'interrio'); ?></label>
            <textarea class="widefat" id="<?php echo esc_attr($this->get_field_id('content')); ?>" name="<?php echo esc_attr($this->get_field_name('content')); ?>" ><?php echo balanceTags($content); ?></textarea>
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('id')); ?>"><?php esc_html_e('Form Id', 'interrio'); ?></label>
            <input placeholder="<?php esc_html_e('id', 'interrio');?>" class="widefat" id="<?php echo esc_attr($this->get_field_id('id')); ?>" name="<?php echo esc_attr($this->get_field_name('id')); ?>" type="text" value="<?php echo esc_attr($id); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title2')); ?>"><?php esc_html_e('Project Title:', 'interrio'); ?></label>
            <input placeholder="<?php esc_html_e('Project Title', 'interrio');?>" class="widefat" id="<?php echo esc_attr($this->get_field_id('title2')); ?>" name="<?php echo esc_attr($this->get_field_name('title2')); ?>" type="text" value="<?php echo esc_attr($title2); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('number')); ?>"><?php esc_html_e('No. of Posts:', 'interrio'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('number')); ?>" name="<?php echo esc_attr($this->get_field_name('number')); ?>" type="text" value="<?php echo esc_attr( $number ); ?>" />
        </p>
       
    	<p>
            <label for="<?php echo esc_attr($this->get_field_id('cat')); ?>"><?php esc_html_e('Category', 'interrio'); ?></label>
            <?php wp_dropdown_categories( array('show_option_all'=>esc_html__('All Categories', 'interrio'), 'selected'=>$cat, 'taxonomy' => 'projects_category', 'class'=>'widefat', 'name'=>$this->get_field_name('cat')) ); ?>
        </p>
                
		<?php 
	}
	
	function posts($args)
	{
		$query = new WP_Query($args);
		if( $query->have_posts() ):?>
        
           	<!-- Title -->
            <ul class="latest-project-items">
				<?php while( $query->have_posts() ): $query->the_post();
				global $post;
				?>
                <?php $post_thumbnail_id = get_post_thumbnail_id($post->ID);
                $post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id ); ?>
            	<li>
                    <div class="img-holder">
                        <?php the_post_thumbnail('interrio_85x85'); ?>
                        <div class="overlay">
                            <div class="box"><div class="content"><a href="http://httest.in/gurukrupa/guru/projects/ <?php //echo esc_url(get_permalink(get_the_id())); ?>"><i class="fa fa-link" aria-hidden="true"></i></a></div></div>
                        </div>
                    </div>
                </li>
            	<?php endwhile; ?>
            </ul>
            
        <?php endif;
		wp_reset_postdata();
    }
	
}
?>
