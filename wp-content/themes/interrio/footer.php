<?php $options = get_option('interrio'.'_theme_options'); ?>
	<div class="clearfix"></div>
    
	<!--Main Footer-->
    <footer class="footer-area">
        <div class="container">
            <div class="row">
				<?php if ( is_active_sidebar( 'footer-sidebar' ) ) { dynamic_sidebar( 'footer-sidebar' ); } ?>
            </div>
        </div>
    </footer>
    
    <!--Start footer bottom area-->   
    <section class="footer-bottom-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                    <div class="copyright-text">
                        <p><?php echo wp_kses_post(interrio_set($options, 'copyrights')); ?></p>
                    </div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                	<?php $social_icons = interrio_set( $options, 'social_media' );
					if(!empty($social_icons)) { ?>
                    <div class="footer-menu">
                        <ul>
                            <?php foreach( interrio_set( $social_icons, 'social_media' ) as $social_icon ):
							if( isset( $social_icon['tocopy' ] ) ) continue; ?>
							<li><a href="<?php echo esc_url(interrio_set( $social_icon, 'social_link')); ?>" target="_blank"><i class="fa <?php echo esc_attr(interrio_set( $social_icon, 'social_icon')); ?>" aria-hidden="true"></i></a></li>
							<?php endforeach; ?>
                        </ul>
                    </div>
					<?php } ?>
                </div>
            </div>    
        </div>
    </section>    
    <!--End footer bottom area-->

</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-angle-up"></span></div>


<?php wp_footer(); ?>


</body>
</html>