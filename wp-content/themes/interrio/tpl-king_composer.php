<?php /* Template Name: KC Page */
	get_header() ;
	$meta = _WSH()->get_meta('_bunch_header_settings');
	$bg = interrio_set($meta, 'header_img');
	$title = interrio_set($meta, 'header_title');
?>
<?php if(interrio_set($meta, 'breadcrumb')):?>
<!--Start breadcrumb area-->     
<section class="breadcrumb-area" <?php if($bg):?>style="background-image:url('<?php echo esc_url($bg)?>');"<?php endif;?>>
	<div class="container-fluid text-center">
		<h1><?php if($title) echo wp_kses_post($title); else wp_title(''); ?></h1>
		<div class="breadcrumb-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="left pull-left">
                            <?php echo balanceTags(interrio_get_the_breadcrumb()); ?>
                        </div>
                        <?php if(interrio_set($meta, 'quote_link')){ ?>
                        <div class="right pull-right">
<?php // echo esc_url(interrio_set($meta, 'quote_link')); ?>
                            <a href="http://www.gurukrupainterior.com/contact-us/"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i><?php esc_html_e('Get a Quote', 'interrio'); ?></a>
                        </div> 
                        <?php } ?>   
                    </div>
                </div>
            </div>
		</div>
	</div>
</section>
<!--End breadcrumb area-->
<?php endif;?>
<?php while( have_posts() ): the_post(); ?>
    <?php the_content(); ?>
<?php endwhile;  ?>
<?php get_footer() ; ?>