<?php interrio_bunch_global_variable(); 

	$options = _WSH()->option();

	get_header(); 

	$settings  = _WSH()->option();

	if(interrio_set($_GET, 'layout_style')) $layout = interrio_set($_GET, 'layout_style'); else

	$layout = interrio_set( $settings, 'archive_page_layout', 'right' );

	if( !$layout || $layout == 'full' || interrio_set($_GET, 'layout_style')=='full' ) $sidebar = ''; else

	$sidebar = interrio_set( $settings, 'archive_page_sidebar', 'blog-sidebar' );

	_WSH()->page_settings = array('layout'=>$layout, 'sidebar'=>$sidebar);

	

	$layout = ($layout) ? $layout : 'right';

	$sidebar = ($sidebar) ? $sidebar : 'blog-sidebar';

	

	$classes = ( !$layout || $layout == 'full' || interrio_set($_GET, 'layout_style')=='full' ) ? ' col-lg-12 col-md-12 col-sm-12 col-xs-12 ' : ' col-lg-9 col-md-8 col-sm-12 col-xs-12 ' ;

	$bg = interrio_set($settings, 'archive_page_header_img');

	$title = interrio_set($settings, 'archive_page_header_title');

	$quote = interrio_set($settings, 'archive_page_header_quote');

?>

<!--Start breadcrumb area-->     

<section class="breadcrumb-area" <?php if($bg):?>style="background-image:url('<?php echo esc_url($bg)?>');"<?php endif;?>>

	<div class="container-fluid text-center">

		<h1><?php if($title) echo wp_kses_post($title); else wp_title(''); ?></h1>

		<div class="breadcrumb-bottom">

            <div class="container">

                <div class="row">

                    <div class="col-md-12">

                        <div class="left pull-left">

                            <?php echo balanceTags(interrio_get_the_breadcrumb()); ?>

                        </div>

                        <?php if($quote){ ?>

                        <div class="right pull-right">

                            <a href="<?php echo esc_url($quote); ?>"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i><?php esc_html_e('Get a Quote', 'interrio'); ?></a>

                        </div> 

                        <?php } ?>

                    </div>

                </div>

            </div>

		</div>

	</div>

</section>

<!--End breadcrumb area-->



<!--Sidebar Page-->

<section id="blog-area" class="blog-with-sidebar-area">

    <div class="container">

        <div class="row">

            

            <!-- sidebar area -->

			<?php if( $layout == 'left' ): ?>

			<?php if ( is_active_sidebar( $sidebar ) ) { ?>

			<div class="col-lg-3 col-md-4 col-sm-7 col-xs-12">        

				<aside class="sidebar-wrapper">

					<?php dynamic_sidebar( $sidebar ); ?>

				</aside>

            </div>

			<?php } ?>

			<?php endif; ?>

            

            <!--Content Side-->	

            <div class="<?php echo esc_attr($classes); ?>">

                

                <!--Default Section-->

                <section class="default-section blog-section no-padd-top no-padd-bottom">

                	<div class="blog-post">

                        <!--Blog Post-->

                        <div class="thm-unit-test">

						<?php while( have_posts() ): the_post();?>

                            <!-- blog post item -->

                            <!-- Post -->

                            <div id="post-<?php the_ID(); ?>" <?php post_class();?>>

                                <?php get_template_part( 'blog' ); ?>

                            <!-- blog post item -->

                            </div><!-- End Post -->

                        <?php endwhile;?>

                    	</div>

                        

                        <!--Pagination-->

                        <div class="row">

                        	<div class="col-md-12">

                                <div class="post-pagination text-center">

                                    <?php interrio_the_pagination(); ?>

                                </div>

                            </div>

                        </div>

                    </div>

                </section>

            </div>

            <!--Content Side-->

            

            <!--Sidebar-->	

            <!-- sidebar area -->

			<?php if( $layout == 'right' ): ?>

			<?php if ( is_active_sidebar( $sidebar ) ) { ?>

			<div class="col-lg-3 col-md-4 col-sm-7 col-xs-12">        

				<aside class="sidebar-wrapper">

					<?php dynamic_sidebar( $sidebar ); ?>

				</aside>

            </div>

			<?php } ?>

			<?php endif; ?>

            <!--Sidebar-->

            

        </div>

    </div>

</section>



<?php get_footer(); ?>