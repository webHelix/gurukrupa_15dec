<?php


/** Set ABSPATH for execution */
define( 'ABSPATH', dirname(dirname(__FILE__)) . '/' );
define( 'WPINC', 'wp-includes' );


/**
 * @ignore
 */
function add_filter() {}

/**
 * @ignore
 */
function esc_attr($str) {return $str;}

/**
 * @ignore
 */
function apply_filters() {}

/**
 * @ignore
 */
function get_option() {}

/**
 * @ignore
 */
function is_lighttpd_before_150() {}

/**
 * @ignore
 */
function add_action() {}

/**
 * @ignore
 */
function did_action() {}

/**
 * @ignore
 */
function do_action_ref_array() {}

/**
 * @ignore
 */
function get_bloginfo() {}

/**
 * @ignore
 */
function is_admin() {return true;}

/**
 * @ignore
 */
function site_url() {}

/**
 * @ignore
 */
function admin_url() {}

/**
 * @ignore
 */
function home_url() {}

/**
 * @ignore
 */
function includes_url() {}

/**
 * @ignore
 */
function wp_guess_url() {}

if ( ! function_exists( 'json_encode' ) ) :
/**
 * @ignore
 */
function json_encode() {}
endif;



/* Convert hexdec color string to rgb(a) string */
 
function hex2rgba($color, $opacity = false) {
 
	$default = 'rgb(0,0,0)';
 
	//Return default if no color provided
	if(empty($color))
          return $default; 
 
	//Sanitize $color if "#" is provided 
        if ($color[0] == '#' ) {
        	$color = substr( $color, 1 );
        }
 
        //Check if color has 6 or 3 characters and get values
        if (strlen($color) == 6) {
                $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
        } elseif ( strlen( $color ) == 3 ) {
                $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
        } else {
                return $default;
        }
 
        //Convert hexadec to rgb
        $rgb =  array_map('hexdec', $hex);
 
        //Check if opacity is set(rgba or rgb)
        if($opacity){
        	if(abs($opacity) > 1)
        		$opacity = 1.0;
        	$output = 'rgba('.implode(",",$rgb).','.$opacity.')';
        } else {
        	$output = 'rgb('.implode(",",$rgb).')';
        }
 
        //Return rgb(a) color string
        return $output;
}
$yellow = $_GET['main_color'];

ob_start(); ?>





.bg-cl-1,
.sec-title span.decor,
.sec-title span.decor:before,
.ui-widget-content .ui-state-focus,
.ui-widget-header .ui-state-focus,
.scroll-to-top,
.header-area .outer-box .cart-btn a .item-count,
.single-service-item .img-holder .overlay .box .content a:hover i,
.latest-project-area .single-project-item .img-holder .overlay .box .content .icon-holder:hover,
.working-area .single-working-item:hover .icon-box,
.working-area .single-working-item:hover .icon-box .count,
.testimonial-area .sec-title .border,
.testimonials-carousel .single-slide-item .img-box .client-photo,
.testimonials-carousel .owl-dots .owl-dot.active,
.single-blog-item .img-holder .overlay .box .content a:hover i,
.brand-area,
.single-footer-widget .subscribe-form form input[type=text]:focus + button, 
.single-footer-widget .subscribe-form form button:hover,
.about-interrio-area .single-item .img-holder .overlay .box .content a:hover i,
.choose-area .sec-title .border,
.choose-area .single-item .icon-holder,
.not-found-content .search-form button,
.not-found-content .search-form input[type="text"]:focus,
.accordion-box .accordion .accord-btn.active:after,
.press-release-item-area .single-item .img-holder .overlay .box .content a:hover i,
.press-release-item-area .single-item:hover .text-holder a,
.blog-with-sidebar-area .blog-post .single-blog-item:hover .text-holder .text .bottom .readmore a,
.blog-with-sidebar-area .blog-post .single-blog-item .text-holder .text .bottom .share .social-share li a:hover i,
.sidebar-wrapper .single-sidebar .search-form button,
.footer-widget form.search-form button,
.blog-single-area .tag-social-share-box .social-share .social-share-links li a:hover i,
.blog-single-area .add-comment-box .add-comment-form button:hover,
.thm-unit-test .comment-area .int-comment-item .default-form button:hover,
#project-area .single-project-item .img-holder .overlay .box .content .icon-holder:hover,
.single-project-item .img-holder .overlay-style-2 .box .content .icon-holder:hover,
#project-grid-area .single-project-item .img-holder .overlay .box .content .icon-holder:hover,
#project-area.pro-withtext-area .single-project-item .img-holder .overlay .box .content .icon-holder:hover,
.project-single-v1-area .project-description-content .project-manager-box::before,
.related-project-items .img-holder .overlay .box .content .icon-holder:hover,
.checkout-area .table .cart-table tbody tr .qty .btn-default,
.placeorder-button button,
.single-shop-item .img-holder .overlay .box .content a:hover,
.single-sidebar.price-filter .noUi-handle,
.single-sidebar.price-filter button,
#shopping-cart-area .cart-middle .apply-coupon .apply-coupon-button button,
#shopping-cart-area .cart-middle .update-cart button:hover,
.product-content-box .content-box .location-box form button:hover,
.service-v2-area .single-item .img-holder .overlay .box .content a,
.appoinment-area form input[type="submit"],
#ui-datepicker-div.ui-datepicker .ui-datepicker-header,
.ui-datepicker-calendar .ui-state-default:hover,
.ui-datepicker-calendar .ui-state-default:active,
.pricing-table-area .single-price-box:hover .table-header,
.pricing-table-area .single-price-box .table-footer a:hover,
.feedback-area .sec-title .border,
.feedback-area .owl-dots .owl-dot.active,
.paginate-links a:hover,
.paginate-links > span,
.post-password-form input[type="submit"],
.main-menu .navbar-header .navbar-toggle .icon-bar,
form.cart button.add-to-cart,
.header-search .search input:focus + button,
.header-search .search input button:hover,
.faq-content-area .question-form form input[type="submit"]:hover,
.brand-area .brand .single-item:hover
{
	background-color: #<?php echo esc_attr($yellow); ?>;
}



.sec-title h2 span,
.top-bar-area .contact-info-left ul li span:before,
.top-bar-area .contact-info-right .phnumber span::before,
.top-social-links ul li a:hover i,
.main-menu .navigation > li:hover > a,
.main-menu .navigation > li.current > a,
.main-menu .navigation > li.current-menu-item > a,
.main-menu .navigation > li.ccurrent-menu-ancestor > a,
.main-menu .navigation > li > ul > li:hover > a,
.main-menu .navigation > li > ul > li.dropdown:hover > a:after,
.main-menu .navigation > li > ul > li  > ul > li:hover > a,
.rev_slider_wrapper .slide-content-box h1 span,
.rev_slider_wrapper .slide-content-box h3 span,
.welcome-area .text-holder .bottom .title h3 span,
.single-service-item .text-holder h3 a,
.latest-project-area .project-filter li.active span, 
.latest-project-area .project-filter li:hover span,
.single-project-item .img-holder .overlay .box .content p,
.single-project-item .img-holder .overlay .box .content h3 a:hover,
.slogan-area .slogan h2,
.working-area .single-working-item .icon-box .icon,
.testimonials-carousel .single-slide-item .review-box ul li i,
.testimonials-carousel .single-slide-item .text-box span::before,
.testimonials-carousel .single-slide-item .text-box .text h4,
.single-blog-item .text-holder .blog-title a:hover,
.blog-with-sidebar-area .single-blog-item .text-holder .blog-title a:hover,
.single-blog-item .text-holder .blog-title:hover,
.single-blog-item .text-holder .meta-info li a:hover,
.single-blog-item .text-holder:hover .text .readmore,
.single-footer-widget .interrio-info p span,
.single-footer-widget .footer-contact-info li .icon-holder span:before,
.single-footer-widget .popular-news .single-popular-news-item .img-holder .overlay .box .content a:hover i,
.single-footer-widget .popular-news .single-popular-news-item .text-holder .info li,
.single-footer-widget .popular-news .single-popular-news-item .text-holder a:hover p,
.single-footer-widget .subscribe-form form button,
.single-footer-widget .latest-project ul li .img-holder .overlay .box .content a:hover i,
.footer-bottom-area .copyright-text p a,
.footer-bottom-area .footer-menu ul li a:hover i,
.breadcrumb-area .breadcrumb-bottom .left ul li a:hover,
.breadcrumb-area .breadcrumb-bottom .right a,
.about-interrio-area .text-holder h3 span,
.about-interrio-area .text-holder .signature-and-name .name p,
.single-team-member .img-holder .overlay .box .content ul li a:hover i,
.single-team-member .text-holder p,
.team-area .owl-theme .owl-nav [class*="owl-"]:hover,
.team-area .bottom-text h3 a,
.fact-counter-area .owl-theme .owl-nav [class*="owl-"]:hover,
.fact-counter-area .single-fact-counter .box h2 span,
.fact-counter-area .single-fact-counter .box h2 i,
.fact-counter-area .single-fact-counter .box .icon-holder span::before,
.accordion-box .accordion .accord-btn.active h4,
.faq-content-area .question-form form input[type="submit"],
.faq-content-area .question-form form input[type="submit"]:hover,
.press-release-content-area .press-release-caption:before,
.press-release-content-area .press-release-caption:after,
.press-release-content-area .client-info .client-name p,
.testimonial-v2-area .single-item .img-holder .quote-box,
.testimonial-v2-area .single-item .text-holder h3 span,
.post-pagination li.active a,
.post-pagination li:hover a,
.post-pagination li span.current,
.post-pagination li span:hover,
.single-sidebar .categories li a:hover,
.single-sidebar .recent-post li .img-holder .overlay .box .content a:hover i,
.single-sidebar .recent-post li .title p,
.single-sidebar .recent-post li .title h3:hover a,
.single-sidebar.widget_tag_cloud .tagcloud a:hover,
.blog-single-area .middle-content-box .text-holder h5,
.blog-single-area .tag-social-share-box .tag a,
.blog-single-area .author-box .text-holder .social-link li a:hover i,
.blog-single-area .comment-box .single-comment-box .text-holder .top .review-box ul li i,
.reply-option a,
.blog-single-area .add-comment-box .add-rating-box ul li a:hover i,
.blog-single-area .add-comment-box .add-comment-form button,
.thm-unit-test .comment-area .int-comment-item .default-form button,
.contact-v1-area .contact-info .single-item .icon-holder span::before,
.contact-v2-area .contact-info .contact-address li .icon-holder span:before,
.contact-v2-area .contact-info .view-on-map a:hover,
#project-area .project-filter li.active span, 
#project-area .project-filter li:hover span,
.single-project-item .img-holder .overlay-style-2 .box .content .text-holder p,
#project-grid-area .project-filter li.active span, 
#project-grid-area .project-filter li:hover span,
#project-area.pro-withtext-area .single-project-item .text-holder p,
#project-single-area .project-info .project-info-list li .icon-holder i,
.project-single-v1-area .project-description-content .project-manager-box h5 span,
.related-project-items .single-project-item .text-holder p,
#project-single-area .bottom .icon-holder a:hover i,
.service-page-area .single-service-item:hover .text-holder .readmore,
.checkout-area .exisitng-customer h5 a,
.checkout-area .coupon h5 a,
.checkout-area .create-acc .checkbox label,
.checkout-area .table .cart-table tbody tr td.price,
.cart-total-table li span.col b,
.cart-total .payment-options .option-block .checkbox label span b,
.account-area .form form .forgot-password a,
.single-shop-item .img-holder .overlay .box .content a,
.review-box ul li i,
.single-shop-item .title-holder h4,
.single-sidebar .recent-products li .img-holder .overlay .box .content a:hover i,
.single-sidebar .recent-products li .title p,
.single-sidebar .recent-products li .title h5:hover a,
.single-sidebar .recent-products li .title .review i,
.shopping-cart-table .table .cart-table tbody tr .remove span:hover::before,
.product-content-box .content-box .review-box ul li i,
.product-content-box .content-box span.price,
.product-content-box .content-box .location-box form span,
.product-tab-box .tab-menu li.active a, .product-tab-box .tab-menu li:hover a,
.product-tab-box .tab-content .review-box .single-review-box .text-holder .top .review-box ul li i,
.product-tab-box .tab-content .review-form .add-rating-box ul li:hover a i,
.design-desc-area .tab-manu .tab-manu-items .single-item.active a,
.design-desc-area .tab-manu .tab-manu-items .single-item:hover a,
.service-v2-area .single-item .text-holder .text h3,
.appoinment-area form .single-field-item .input-box .iocn-holder i,
.appoinment-area p.customercare span,
.appoinment-area .business-hours h3 i,
.feedback-area .single-feedback-item .text-holder h3 span,
.single-sidebar ul li a:hover,
a,
.woocommerce .star-rating span,
.woocommerce div.product .woocommerce-tabs ul.tabs li.active a,
.woocommerce div.product .woocommerce-tabs ul.tabs li:hover a,
.woocommerce .woocommerce-message::before
{
	color: #<?php echo esc_attr($yellow); ?>;
}



.thm-btn,
.thm-btn:hover,
.scroll-to-top,
.header-search button,
.header-search .search input:focus,
.main-menu .navigation > li > ul,
.main-menu .navigation > li > ul > li > ul,
.testimonials-carousel .owl-dots .owl-dot.active,
.single-footer-widget .popular-news .single-popular-news-item .text-holder .info li,
.single-footer-widget .subscribe-form form input[type=text]:focus,
.single-footer-widget .subscribe-form form input[type=text]:focus + button, 
.single-footer-widget .subscribe-form form button:hover,
.team-area .owl-theme .owl-nav [class*="owl-"]:hover,
.fact-counter-area, 
.owl-theme .owl-nav [class*="owl-"]:hover,
.blog-with-sidebar-area .blog-post .single-blog-item .text-holder .text .bottom .share .social-share li a:hover i,
.blog-single-area .tag-social-share-box .social-share .social-share-links li a:hover i,
.blog-single-area .add-comment-box .add-comment-form input[type="text"]:focus,
.blog-single-area .add-comment-box .add-comment-form textarea:focus,
.thm-unit-test .comment-area .int-comment-item .default-form textarea:focus,
.contact-v1-area .contact-form-v1 form input[type="text"]:focus,
.contact-v1-area .contact-form-v1 form input[type="email"]:focus,
.contact-v1-area .contact-form-v1 form textarea:focus,
.contact-v2-form-area .contact-v2-form form input[type="text"]:focus,
.contact-v2-form-area .contact-v2-form form input[type="email"]:focus,
.contact-v2-form-area .contact-v2-form form textarea:focus,
.project-single-v1-area .project-description-content .project-manager-box,
.checkout-area .form form .field-input input[type="text"]:focus,
.checkout-area .form form .field-input textarea:focus,
.placeorder-button button,
.account-area .form form .field-input input[type="text"]:focus,
#shopping-cart-area .cart-middle .apply-coupon input:focus,
#shopping-cart-area .cart-middle .update-cart button:hover,
.product-content-box .flex-control-thumbs .flex-active,
.product-content-box .content-box .location-box form input:focus,
.product-tab-box .tab-content .review-form form input[type="text"]:focus,
.product-tab-box .tab-content .review-form form textarea:focus,
.service-v2-area .single-item .img-holder .overlay .box .content a,
.appoinment-area form input[type="submit"],
.appoinment-area form .single-field-item input[type="text"]:focus,
.ui-datepicker-calendar .ui-state-default:hover,
.ui-datepicker-calendar .ui-state-default:active,
.pricing-table-area .single-price-box:hover .price,
.pricing-table-area .single-price-box:hover .price-list,
.pricing-table-area .single-price-box:hover .table-footer,
.pricing-table-area .single-price-box .table-footer a:hover,
.feedback-area .owl-dots .owl-dot.active,
.paginate-links a,
.paginate-links > span,
.paginate-links a:hover,
.paginate-links > span,
.post-password-form input[type="password"],
.main-menu .navbar-header .navbar-toggle,
form.cart button.add-to-cart,
form.cart button.add-to-cart:hover,
.woocommerce-cart table.cart td.actions .coupon .input-text:focus,
.woocommerce .woocommerce-message,
.scroll-to-top:hover,
.header-search .search input:focus + button,
.header-search .search input button:hover,
.fact-counter-area .owl-theme .owl-nav [class*="owl-"]:hover,
.brand-area .brand .single-item,
.brand-area .brand .single-item:hover
{
	border-color: #<?php echo esc_attr($yellow); ?>;
}



.woocommerce div.product p.price,
.woocommerce div.product span.price,
.woocommerce div.product .price ins span,
.woocommerce-pagination ul li a:hover,
.woocommerce-pagination ul li span,
.woocommerce nav.woocommerce-pagination ul li a:focus,
.woocommerce nav.woocommerce-pagination ul li a:hover,
.woocommerce nav.woocommerce-pagination ul li span.current
{
	color: #<?php echo esc_attr($yellow); ?> !important;
}



.woocommerce #respond input#submit,
.woocommerce a.button,
.woocommerce button.button,
.woocommerce input.button,
.woocommerce #place_order,
.woocommerce #respond input#submit:hover,
.woocommerce a.button:hover,
.woocommerce button.button:hover,
.woocommerce input.button:hover,
.woocommerce #place_order:hover
{
	border-color: #<?php echo esc_attr($yellow); ?> !important;
}



.woocommerce #respond input#submit,
.woocommerce a.button,
.woocommerce button.button,
.woocommerce input.button,
.woocommerce #place_order
{
    background-color: #<?php echo esc_attr($yellow); ?> !important;
}


.welcome-area .gallery .video-gallery:hover .overlay-gallery
{
    background-color: <?php echo hex2rgba($yellow, 0.7);?> !important;
}





.slogan-area.bg-clr-1::before,
.slogan-v2-area::before
{
    background-color: <?php echo hex2rgba($yellow, 0.9);?> !important;
}

@media only screen and (max-width: 767px) {
.main-menu .navbar-collapse > .navigation > li > a,
.main-menu .navbar-collapse > .navigation > li > ul > li > a,
.main-menu .navbar-collapse > .navigation > li > ul > li > ul > li > a{
	background-color: #<?php echo esc_attr($yellow); ?> !important;
}
.main-menu .navigation > li:hover > a, .main-menu .navigation > li.current > a, .main-menu .navigation > li.current-menu-item > a, .main-menu .navigation > li.ccurrent-menu-ancestor > a{
	color: #ffffff;
}
}
<?php 

$out = ob_get_clean();
$expires_offset = 31536000; // 1 year
header('Content-Type: text/css; charset=UTF-8');
header('Expires: ' . gmdate( "D, d M Y H:i:s", time() + $expires_offset ) . ' GMT');
header("Cache-Control: public, max-age=$expires_offset");
header('Vary: Accept-Encoding'); // Handle proxies
header('Content-Encoding: gzip');

echo gzencode($out);
exit;