<?php
/**
 * The Template for displaying all single products.
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
$meta = _WSH()->get_meta('_bunch_layout_settings');
$meta1 = _WSH()->get_meta('_bunch_header_settings');

$layout = interrio_set( $meta, 'layout', 'right' );
$layout = interrio_set( $_GET, 'layout' ) ? $_GET['layout'] : $layout; 
if(interrio_set($_GET, 'layout_style'))
$layout = interrio_set($_GET, 'layout_style');
else
$layout = interrio_set( $meta, 'layout', 'right' );
$sidebar = interrio_set( $meta, 'sidebar', 'blog-sidebar' );

$layout = ($layout) ? $layout : 'right';
$sidebar = ($sidebar) ? $sidebar : 'blog-sidebar';

$classes = ( !$layout || $layout == 'full' || interrio_set($_GET, 'layout_style')=='full' ) ? ' col-lg-12 col-md-12 col-sm-12 col-xs-12 ' : ' col-lg-9 col-md-8 col-sm-12 col-xs-12 ' ;
$bg = interrio_set($meta1, 'header_img');
$title = interrio_set($meta1, 'header_title');
get_header( 'shop' ); ?>

<!--Start breadcrumb area-->     
<section class="breadcrumb-area" <?php if($bg):?>style="background-image:url('<?php echo esc_url($bg)?>');"<?php endif;?>>
	<div class="container-fluid text-center">
		<h1><?php if($title) echo balanceTags($title); else wp_title(''); ?></h1>
		<div class="breadcrumb-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="left pull-left">
                            <?php echo balanceTags(interrio_get_the_breadcrumb()); ?>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</section>
<!--End breadcrumb area-->

<section class="shop-single-area">
    <div class="container">
        <div class="row">
			
            <!-- sidebar area -->
			<?php if( $layout == 'left' ): ?>
			<?php if ( is_active_sidebar( $sidebar ) ) { ?>
			<div class="col-lg-3 col-md-4 col-sm-7 col-xs-12">        
				<aside class="sidebar-wrapper">
					<?php dynamic_sidebar( $sidebar ); ?>
                    <?php
							/**
							 * woocommerce_sidebar hook
							 *
							 * @hooked woocommerce_get_sidebar - 10
							 */
							do_action( 'woocommerce_sidebar' );
						?>
				</aside>
            </div>
			<?php } ?>
			<?php endif; ?>
			
            <div class="<?php echo esc_attr($classes);?> <?php if($layout == 'full'){ echo 'single-products-details-full'; } elseif($layout == 'right'){ echo 'single-products-details'; } elseif($layout == 'left'){ echo 'single-products-details-left'; } ?>">
            
            <?php
                /**
                 * woocommerce_before_main_content hook
                 *
                 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
                 * @hooked woocommerce_breadcrumb - 20
                 */
                do_action( 'woocommerce_before_main_content' );
            ?>
                <?php while ( have_posts() ) : the_post(); ?>
                    <?php wc_get_template_part( 'content', 'single-product' ); ?>
                <?php endwhile; // end of the loop. ?>
            <?php
                /**
                 * woocommerce_after_main_content hook
                 *
                 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
                 */
                do_action( 'woocommerce_after_main_content' );
            ?>
            
            </div>
		
        	<!-- sidebar area -->
			<?php if( $layout == 'right' ): ?>
			<?php if ( is_active_sidebar( $sidebar ) ) { ?>
			<div class="col-lg-3 col-md-4 col-sm-7 col-xs-12">        
				<aside class="sidebar-wrapper">
					<?php dynamic_sidebar( $sidebar ); ?>
                    <?php
							/**
							 * woocommerce_sidebar hook
							 *
							 * @hooked woocommerce_get_sidebar - 10
							 */
							do_action( 'woocommerce_sidebar' );
						?>
				</aside>
            </div>
			<?php } ?>
			<?php endif; ?>
		</div>
	</div>
</section>
<?php get_footer( 'shop' ); ?>
